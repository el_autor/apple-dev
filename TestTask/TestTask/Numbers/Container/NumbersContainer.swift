import UIKit

final class NumbersContainer {
	let viewController: UIViewController
	private(set) weak var router: NumbersRouterInput!

	class func assemble(with context: NumbersContext) -> NumbersContainer {
        let router = NumbersRouter()
        let interactor = NumbersInteractor(type: context.type)
        let presenter = NumbersPresenter(router: router, interactor: interactor)
        let viewController = NumbersViewController()

        viewController.output = presenter
        presenter.view = viewController
        interactor.output = presenter
        router.viewController = viewController

        return NumbersContainer(view: viewController, router: router)
	}

    private init(view: UIViewController, router: NumbersRouterInput) {
		self.viewController = view
		self.router = router
	}
}

struct NumbersContext {
    var type: ScreenType
}

enum ScreenType {
    case simple
    case fibonacci
}

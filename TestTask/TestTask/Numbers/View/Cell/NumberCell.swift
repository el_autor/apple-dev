import UIKit
import PinLayout

final class NumberCell: UICollectionViewCell {
    
    private weak var numberLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)

        setupSubviews()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        layoutNumberLabel()
    }
    
    private func setupSubviews() {
        setupNumberLabel()
    }
    
    private func setupNumberLabel() {
        let label = UILabel()
        
        numberLabel = label
        addSubview(numberLabel)
        
        numberLabel.font = .boldSystemFont(ofSize: 30)
        numberLabel.textAlignment = .center
        numberLabel.textColor = .black
    }
    
    private func layoutNumberLabel() {
        numberLabel.pin
            .all()
    }
}

extension NumberCell {
    func configure(number: String) {
        numberLabel.text = number
    }
}

struct CellModel {
    var number: String
}

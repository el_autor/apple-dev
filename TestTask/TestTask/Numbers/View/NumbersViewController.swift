import UIKit
import PinLayout

final class NumbersViewController: UIViewController {
	var output: NumbersViewOutput?

    private weak var numbersCollectionView: UICollectionView!
    
    private var isLoading = false
    
	override func viewDidLoad() {
		super.viewDidLoad()
        
        setupView()
        setupSubviews()
	}
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        layoutCollectionView()
    }
    
    private func setupView() {
        view.backgroundColor = .white
    }
    
    private func setupSubviews() {
        setupCollectionView()
    }
    
    private func setupCollectionView() {
        let flowLayout = UICollectionViewFlowLayout()
        
        flowLayout.scrollDirection = .vertical
        flowLayout.minimumInteritemSpacing = .zero
        flowLayout.minimumLineSpacing = .zero
    
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: flowLayout)
        
        numbersCollectionView = collectionView
        view.addSubview(collectionView)
        
        numbersCollectionView.delegate = self
        numbersCollectionView.dataSource = self
        
        numbersCollectionView.register(NumberCell.self,
                                       forCellWithReuseIdentifier: "NumbersCell")
        
        numbersCollectionView.showsVerticalScrollIndicator = false
        numbersCollectionView.backgroundColor = .white
        numbersCollectionView.translatesAutoresizingMaskIntoConstraints = false

    }
    
    private func layoutCollectionView() {
        numbersCollectionView.pin
            .all()
    }
}

extension NumbersViewController: NumbersViewInput {
}

extension NumbersViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row == (output?.getSize() ?? .zero) - 20 && !self.isLoading {
            loadMoreData()
        }
    }
    
    func loadMoreData() {
        if !self.isLoading {
            self.isLoading = true
            
            let start = (output?.getSize() ?? .zero)
            let end = start + 20
            
            DispatchQueue.global().async {
                for index in start...end {
                    self.output?.addNumber(index: index)
                }
                
                DispatchQueue.main.async {
                    self.numbersCollectionView.reloadData()
                    self.isLoading = false
                }
            }
        }
    }
}

extension NumbersViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return output?.getSize() ?? .zero
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NumbersCell", for: indexPath) as? NumberCell else {
            return UICollectionViewCell()
        }

        if indexPath.row == .zero {
            cell.backgroundColor = .white
        } else if (((indexPath.row - 1) / 2) % 2 == 0) {
            cell.backgroundColor = .gray
        } else {
            cell.backgroundColor = .white
        }
        
        guard let model = output?.viewModel(index: indexPath.row) else {
            return cell
        }
        
        cell.configure(number: model.number)
        
        return cell

    }
}

extension NumbersViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width / 2, height: UIScreen.main.bounds.height / 6)
    }
}

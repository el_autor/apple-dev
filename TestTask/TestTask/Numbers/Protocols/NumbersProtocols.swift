import Foundation

protocol NumbersViewInput: AnyObject {
}

protocol NumbersViewOutput: AnyObject {
    func getSize() -> Int
    
    func viewModel(index: Int) -> CellModel
    
    func addNumber(index: Int)
}

protocol NumbersInteractorInput: AnyObject {
    func generateNumbers() -> [Int]
    
    func addNumber(numbers: inout [Int], index: Int)
}

protocol NumbersInteractorOutput: AnyObject {
}

protocol NumbersRouterInput: AnyObject {
}

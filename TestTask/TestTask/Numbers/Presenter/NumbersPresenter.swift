import Foundation

final class NumbersPresenter {
	weak var view: NumbersViewInput?

	private let router: NumbersRouterInput
	private let interactor: NumbersInteractorInput
    
    private var numbers: [Int]
    
    init(router: NumbersRouterInput, interactor: NumbersInteractorInput) {
        self.router = router
        self.interactor = interactor
    
        self.numbers = interactor.generateNumbers()
    }
}

extension NumbersPresenter: NumbersViewOutput {
    func getSize() -> Int {
        return numbers.count
    }
    
    func viewModel(index: Int) -> CellModel {
        return CellModel(number: String(numbers[index]))
    }
    
    func addNumber(index: Int) {
        interactor.addNumber(numbers: &numbers, index: index)
    }
}

extension NumbersPresenter: NumbersInteractorOutput {
}



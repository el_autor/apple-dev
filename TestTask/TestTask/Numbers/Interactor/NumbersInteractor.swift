import Foundation

final class NumbersInteractor {
	weak var output: NumbersInteractorOutput?
    
    var type: ScreenType
    
    init(type: ScreenType) {
        self.type = type
    }
}

extension NumbersInteractor: NumbersInteractorInput {
    func generateNumbers() -> [Int] {
        if type == .fibonacci {
            var fibonacci: [Int] = [0, 1]
            
            // Последовательность Фибоначчи вылезает за Int.max очень быстро поэтому генерирую только 50 членов, а дальше по кольцу
            
            for i in 2..<50 {
                fibonacci.append(fibonacci[i - 1] + fibonacci[i - 2])
            }
            
            return fibonacci
        } else {
            var simple: [Int] = [0, 1]
            
            for _ in .zero..<48 {
                addNumber(numbers: &simple, index: .zero)
            }
            
            return simple
        }
    }
    
    func addNumber(numbers: inout [Int], index: Int) {
        if type == .fibonacci {
            numbers.append(numbers[index % 50])
        } else {
            var last = numbers.last ?? .zero
            
            while true {
                var flag = true
                last = last + 1
                
                for i in 1...(last / 2) {
                    if last % i == 0 && i != 1 {
                        flag = false
                        break
                    }
                }
                
                if flag {
                    numbers.append(last)
                    return
                }
            }
        }
    }
}

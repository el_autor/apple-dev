class Node<T> {
    var value: T

    var next: Node<T>?
    weak var previous: Node<T>?

    init(value: T) {
        self.value = value
    }
}

class LinkedList<T> {
    var head: Node<T>?
    var tail: Node<T>?

    var isEmpty: Bool {
        return head == nil
    }

    func append(value: Node<T>) {
        if let tailNode = tail {
            value.previous = tailNode
            tailNode.next = value
        } else {
            head = value
        }

        tail = value
    }

    func append(contentsOf: [Node<T>]) {
        for i in 0..<contentsOf.count {
            append(value: contentsOf[i])
        }
    }

    func popFirst() -> Node<T>? {
        let node = head

        head = node?.next

        if isEmpty {
            tail = nil
        }

        return node
    }

    init(array: [Node<T>]) {
        for i in 0..<array.count {
            append(value: array[i])
        }
    }
}


let n = Int(readLine()!)!

var cities: [(Int, Int)] = []

for _ in 0..<n {
    let city = readLine()!
    .split { $0 == " " }
    .map { Int($0)! }

    cities.append((city[0], city[1]))
}

let maxWithoutRefill = Int(readLine()!)!
let trip: [Int] = readLine()!
                    .split { $0 == " " }
                    .map { Int($0)! }

let cityFrom = trip[0]
let cityTo = trip[1]

var tripGraph: [Int: [Int]] = [:]

for i in 0..<n {
    var closestCities: [Int] = []

    for j in 0..<n {
        if i != j {
            let distance = abs(cities[i].0 - cities[j].0) + abs(cities[i].1 - cities[j].1)

            if distance <= maxWithoutRefill {
                closestCities.append(j)
            }
        }
    }

    tripGraph[i] = closestCities
}

var visitedCities: Set<Int> = []
var queue = LinkedList<(Int, Int)>(array: [Node<(Int, Int)>(value: (cityFrom - 1, 0))])
var flag = false

while !queue.isEmpty {
    guard let first = queue.popFirst() else {
        break
    }

    if !visitedCities.contains(first.value.0) {
        if first.value.0 == cityTo - 1 {
            print(first.value.1)
            flag = true
            break
        } else {
            queue.append(contentsOf: tripGraph[first.value.0]!.map { Node<(Int, Int)>(value: ($0, first.value.1 + 1)) })
        }

        visitedCities.insert(first.value.0)
    }
}

if !flag {
    print(-1)
}


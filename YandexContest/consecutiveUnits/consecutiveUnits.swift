let n = Int(readLine()!)!

var vector: [Int] = []

for _ in 0..<n {
    let numeric = Int(readLine()!)!

    vector.append(numeric)
}

var maxOnes = 0
var currentOnes = 0
var currentZeros = 0

for i in 0..<n {
    if vector[i] == 1 {
        if currentZeros == 0 {
            currentOnes += 1
        } else {
            currentOnes = 1
        }

        currentZeros = 0
    } else {
        if currentOnes == 0 {
            currentZeros += 1
        } else {
            currentZeros = 1
        }

        currentOnes = 0
    }

    if currentOnes > maxOnes {
        maxOnes = currentOnes
    }
}

print(maxOnes)
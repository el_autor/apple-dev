let n = Int(readLine()!)!

var vector: [Int] = []

for _ in 0..<n {
    vector.append(Int(readLine()!)!)
}

var unique = Set(vector)
vector = Array(unique)
vector.sort { $0 < $1 }

for i in 0..<vector.count {
    print(vector[i])
}
struct ParSequence {
    var currentString: String

    var leftSidedDidSet: Int

    var rightSidedDidSet: Int
}

let n = Int(readLine()!)!

if n != 0 {
    var result: [ParSequence] = [ParSequence(currentString: "(",
                                             leftSidedDidSet: 1,
                                             rightSidedDidSet: 0)]

    for _ in 0..<2 * n {
        let currentResultCount = result.count

        for i in 0..<currentResultCount {
            var sequenceCopy = result[i]
            var flag = true

            if result[i].leftSidedDidSet < n {
                result[i].currentString += "("
                result[i].leftSidedDidSet += 1
            } else if result[i].leftSidedDidSet > result[i].rightSidedDidSet {
                flag = false
                result[i].currentString += ")"
                result[i].rightSidedDidSet += 1
            }

            if flag && sequenceCopy.leftSidedDidSet > sequenceCopy.rightSidedDidSet {
                sequenceCopy.currentString += ")"
                sequenceCopy.rightSidedDidSet += 1
                result.append(sequenceCopy)
            }
        }
    }

    result.sort{ $0.currentString < $1.currentString }

    for i in 0..<result.count {
        print(result[i].currentString)
    }
}
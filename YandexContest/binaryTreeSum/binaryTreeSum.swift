class Node {
    var value: Int

    var leftChild: Node?
    var rightChild: Node?

    init(value: Int) {
        self.value = value
    }
}

func getSegmentSum(root: Node, leftBorder: Int, rightBorder: Int) -> Int {
    var sum: Int = 0
    var currentIndex: Int = 0

    checkNode(node: root,
              leftBorder: leftBorder,
              rightBorder: rightBorder,
              currentIndex: &currentIndex,
              sum: &sum)

    return sum
}

func checkNode(node: Node?, leftBorder: Int, rightBorder: Int, currentIndex: inout Int, sum: inout Int) {
    guard let node = node else {
        return
    }

    checkNode(node: node.leftChild,
              leftBorder: leftBorder,
              rightBorder: rightBorder,
              currentIndex: &currentIndex,
              sum: &sum)

    if (leftBorder...rightBorder).contains(currentIndex) {
        sum += node.value
    }
    currentIndex += 1

    checkNode(node: node.rightChild,
              leftBorder: leftBorder,
              rightBorder: rightBorder,
              currentIndex: &currentIndex,
              sum: &sum)
} 

var node1 = Node(value: 2)
var node2 = Node(value: 4)
var node3 = Node(value: 6)
var node4 = Node(value: 8)
var node5 = Node(value: 10)
var node6 = Node(value: 20)

node4.leftChild = node2
node4.rightChild = node5

node5.rightChild = node6

node2.leftChild = node1
node2.rightChild = node3

print(getSegmentSum(root: node4, leftBorder: -1, rightBorder: 5))
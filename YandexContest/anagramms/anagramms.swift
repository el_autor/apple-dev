let a = readLine()!
let b = readLine()!

var dictA: [Character: Int] = [:]
var dictB: [Character: Int] = [:]

if a.count != b.count {
    print(0)
} else {
    for letter in a {
        if !dictA.keys.contains(letter) {
            dictA[letter] = 1
        } else {
            dictA[letter]! += 1
        }
    }

    for letter in b {
        if !dictB.keys.contains(letter) {
            dictB[letter] = 1
        } else {
            dictB[letter]! += 1
        }
    }

    if dictA.count != dictB.count {
        print(0)
    } else {
        var flag = true

        for (key, value) in dictA {
            if !dictB.keys.contains(key) {
                flag = false
                print(0)
                break
            }

            if dictB[key]! != value {
                flag = false
                print(0)
                break
            }
        }

        if flag {
            print(1)
        }
    }
}
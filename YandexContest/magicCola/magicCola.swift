var n = Int(readLine()!)!

let letters = ["Т", "М", "А", "Л", "Д"]

var offset = letters.count
var letterRepeats = 1

while n >= offset {
    n -= offset
    letterRepeats += 1
    offset = letters.count * letterRepeats
}

print(letters[n / letterRepeats])
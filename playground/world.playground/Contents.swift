import Foundation

/*
var str = "Hello, playground"
var a = 5

print(str)
print("Hello, mumbase!!!")
print("Bye!")

var integer = 33
integer += 1
print(integer)

var ff = 1230
print(ff)
*/

/*
var first: Int = 100
var second: Int = 200

let third: Int = first * second + 1

print(first * second)
print(third)
*/

/*
var first: Double = 100.01
var second: Double = 10.0

print(first / second)
print(first.truncatingRemainder(dividingBy: second))
*/

/*
var first: Int = 5
var second: Double = 10.123

var third: Double = second + Double(first)

print(third)
*/

/*
var first: Int = 3
let second: Int = 55

switch first
{
case 1:
    print(first)
case 2:
    print(second)
case 3:
    print(44)
default:
    print(12334)
}
*/

/*
let value: Int = -1

switch value
{
case 0...5:
    print("here")
default:
    print("not here")
}
*/

/*
let cortege = (0, 1)

switch cortege
{
case (0, 0):
    print(1)
case (_, 1): // proper
    print(2)
case (0, -10...10):
    print(3)
default:
    print(4)
}
*/

/*
let value: Int = 10

switch value
{
case 0:
    print(0)
case 1, 3, 5, 7, 9:
    print(1)
case 2, 4, 6, 8, 10:
    print(2)
default:
    break
}
*/

/*
var value: Int = 10

guard value = 10
else
{
    print("not equal")
    return
}
*/

/*
let numbers = [1, 2, 3, 4, 5, 6, 8, 9]

for number in numbers
{
    if (number % 2 == 0)
    {
        continue
    }
    
    print(number * 2)
}
*/

var value: Int = 4

switch value
{
case 1...5:
    value *= 2
    fallthrough
case 5...10:
    value += 1
    fallthrough
default:
    value *= 0
}

print(value)




//
//  ViewController.swift
//  UIElements2
//
//  Created by Vlad on 02.08.2020.
//  Copyright © 2020 Vlad Krivozubov. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var stepper: UIStepper!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var progressBar: UIProgressView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textView.delegate = self
        textView.text = "12345678910"
        textView.isHidden = true
        textView.alpha = 0
        
        textView.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 17)
        //textView.backgroundColor = .systemGreen
        textView.backgroundColor = self.view.backgroundColor
        textView.layer.cornerRadius = 10
        //textView.keyboardDismissMode = .none
        
        // Уведомление о появлении клавиатуры
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(updateTextView(notification:)),
                                               name: UIResponder.keyboardWillChangeFrameNotification,
                                               object: nil)
        // Уведомление о скрытии клавиатуры
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(updateTextView(notification:)),
                                               name: UIResponder.keyboardWillHideNotification,
                                               object: nil)
        
        stepper.value = 17
        stepper.minimumValue = 10
        stepper.maximumValue = 25
        
        stepper.tintColor = .white
        stepper.backgroundColor = .gray
        stepper.layer.cornerRadius = 5
        
        activityIndicator.hidesWhenStopped = true
        activityIndicator.color = .darkGray
        activityIndicator.startAnimating()
        // заморозка интерфейса на время загрузки
        self.view.isUserInteractionEnabled = false
        
        UIView.animate(withDuration: 0,
                       delay: 5,
                       options: .curveEaseIn,
                       animations: {
                        self.textView.alpha = 1
        },
                       completion: {
                        processed in
                        self.activityIndicator.stopAnimating()
                        self.textView.isHidden = false
                        self.view.isUserInteractionEnabled = true
        })
        
        progressBar.setProgress(0, animated: true)
        
        Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { _ in
            if self.progressBar.progress != 1 {
                self.progressBar.progress += 0.2
            }
            else {
                self.progressBar.isHidden = true
            }
        })
    }
    
    @objc func updateTextView(notification: Notification) {
        
        guard let userInfo = notification.userInfo as? [String: AnyObject],
            let keyboardFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }
        
        if notification.name == UIResponder.keyboardWillHideNotification {
            textView.contentInset = UIEdgeInsets.zero
        }
        else {
            textView.contentInset = UIEdgeInsets(top: 0,
                                                 left: 0,
                                                 bottom: keyboardFrame.height - bottomConstraint.constant,
                                                 right: 0)
            //textView.scrollIndicatorInsets = textView.contentInset
        }
        
        //textView.scrollRangeToVisible(textView.selectedRange)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        
        self.view.endEditing(true)
        
        //textView.resignFirstResponder() // для одного элемента
    }
    
    @IBAction func changeFont(_ sender: UIStepper) {
        let font = textView.font?.fontName
        let fontSize = CGFloat(sender.value)
        
        textView.font = UIFont(name: font!, size: fontSize)
    }
    
}

extension ViewController: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.backgroundColor = .white
        textView.textColor = .gray
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        textView.backgroundColor = self.view.backgroundColor
        textView.textColor = .black
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        label.text = "\(textView.text.count + text.count - range.length)"
        return textView.text.count + text.count - range.length < 30
    }
    
}

//
//  APIManager.swift
//  WeatherApp
//
//  Created by Vlad on 18.08.2020.
//  Copyright © 2020 Vlad Krivozubov. All rights reserved.
//

import Foundation

enum APIResult<T> {
    case Success(T)
    case Failure(Error)
}

protocol JSONDecodable {
    init?(JSON: [String: AnyObject])
}

protocol FinalURLPoint {
    var baseURL: URL { get }
    var path: String { get }
    var request: URLRequest { get }
}

typealias JSONTask = URLSessionDataTask
typealias JSONCompletionHandler = ([String: AnyObject]?, HTTPURLResponse?, Error?) -> Void

protocol APIManager {
    var sessionConfiguration: URLSessionConfiguration { get }
    var session: URLSession { get }
    
    func JSONTaskWith(request: URLRequest, completionHanlder: JSONCompletionHandler) -> JSONTask
    func fetch<T: JSONDecodable>(request: URLRequest, parse: ([String: AnyObject]) -> T?, completionHandler: (APIResult<T>) -> Void)
}

// default implementation

extension APIManager {
    func JSONTaskWith(request: URLRequest, completionHanlder: @escaping JSONCompletionHandler) -> JSONTask {
        let dataTask = session.dataTask(with: request) { (data, response, error) in
            guard let HTTPResponse = response as? HTTPURLResponse else {
                
                let userInfo = [NSLocalizedDescriptionKey: NSLocalizedString("Missing HTTP response", comment: "")]
                let error = NSError(domain: SWINetworkingErrorDomain, code: 100, userInfo: userInfo)
                
                completionHanlder(nil, nil, error)
                
                return
            }
            
            if data == nil {
                if let error = error {
                    completionHanlder(nil, HTTPResponse, error)
                }
            }
            else {
                switch HTTPResponse.statusCode {
                case 200:
                    do {
                        let json = try JSONSerialization.jsonObject(with: data!, options: []) as? [String: AnyObject]
                        completionHanlder(json, HTTPResponse, nil)
                    }
                    catch let error as NSError {
                        completionHanlder(nil, HTTPResponse, error)
                    }
                default:
                    print("We got response status \(HTTPResponse.statusCode)")
                }
            }
        }
        
        return dataTask
    }
    
    func fetch<T>(request: URLRequest, parse: ([String: AnyObject]) -> T?, completionHandler: (APIResult<T>) -> Void) {
        let dataTask = JSONTaskWith(request: request) { (json, response, error) in
            guard let json = json else {
                if let error = error {
                    completionHandler(.Failure(error))
                }
                
                return
            }
            
            if let value = parse(json) {
                completionHandler(.Success(value))
            }
            else {
                let error = NSError(domain: SWINetworkingErrorDomain, code: 200, userInfo: nil)
                completionHandler(.Failure(error))
            }
        }
        
        dataTask.resume()
    }
}

//
//  ErrorManager.swift
//  WeatherApp
//
//  Created by Vlad on 18.08.2020.
//  Copyright © 2020 Vlad Krivozubov. All rights reserved.
//

import Foundation

public let SWINetworkingErrorDomain = "ru.swiftbook.WeatherApp.NetworkingError"
public let MissingHTTPResponseError = 100
public let UnexpectedResponseError = 200

//
//  ViewController.swift
//  WeatherApp
//
//  Created by Vlad on 11.08.2020.
//  Copyright © 2020 Vlad Krivozubov. All rights reserved.
//

import UIKit
import CoreLocation

class ViewController: UIViewController, CLLocationManagerDelegate {
    
    @IBOutlet weak var locataionLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var pressureLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var apparentTemperatureLabel: UILabel!
    @IBOutlet weak var refreshButton: UIButton!
    @IBOutlet weak var waiting: UIActivityIndicatorView!
    
    let locationManager = CLLocationManager()
    
    lazy var weatherManager = APIWeatherManager(apiKey: "sdfgfgjfgdsreescghbfddx")
    let coordinates = Coordinates(latitute: 54.000000, longitude: 55.000000)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        
        toggleActivityIndicator(on: true)
        getCurrentWeatherData()
        
        //let urlString = "https://api.forecast.io/forecast/token/coord1,coord2"
        
        // unsafe code
        
        /*
        let baseUrl = URL(string: "https://api.forecast.io/forecast/token/")
        let fullURL = URL(string: "coord1,coord2", relativeTo: baseUrl)
        
        let sessionConfiguration = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfiguration)
        
        let request = URLRequest(url: fullURL!)
        let dataTask = session.dataTask(with: fullURL!) { (data, response, error) in
            
        }
        
        dataTask.resume()
        */
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation = locations.last! as CLLocation
        
        print("My location latititude: \(userLocation.coordinate.latitude), longtitide \(userLocation.coordinate.longitude)")
    }
    
    func toggleActivityIndicator(on: Bool) {
        refreshButton.isHidden = on
        
        if on {
            waiting.startAnimating()
        }
        else {
            waiting.stopAnimating()
        }
    }
    
    func getCurrentWeatherData() {
        weatherManager.fetchCurrentWeatherWith(coordinates: self.coordinates, completionHandler: { (result) in
             switch result {
             case .Success(let currentWeather):
                 updateUIWith(currentWeather: currentWeather)
             case .Failure(let error as NSError):
                 let alertController = UIAlertController(title: "Unable to get data", message: "\(error.description)", preferredStyle: .alert)
                 let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                 alertController.addAction(okAction)
                 
                 self.present(alertController, animated: true, completion: nil)
             }
         })
    }

    func updateUIWith(currentWeather: CurrentWeather) {
        
        imageView.image = currentWeather.icon
        pressureLabel.text = currentWeather.pressureString
        humidityLabel.text = currentWeather.humidityString
        temperatureLabel.text = currentWeather.temperatureString
        apparentTemperatureLabel.text = currentWeather.ApparentTemperatureString
        
    }
    
    @IBAction func refreshButtonPressed(_ sender: UIButton) {
        toggleActivityIndicator(on: false)
        getCurrentWeatherData()
    }
    
}

//
//  APIWeatherManager.swift
//  WeatherApp
//
//  Created by Vlad on 22.08.2020.
//  Copyright © 2020 Vlad Krivozubov. All rights reserved.
//

import Foundation

struct Coordinates {
    let latitute: Double
    let longitude: Double
}

enum ForecastType: FinalURLPoint {
    var baseURL: URL {
        return URL(string: "https://api.forecast.io/")!
    }
    
    var path: String {
        switch self {
        case .Current(let apiKey, let coordinates):
            return "/forecast/\(apiKey)/\(coordinates.latitute),\(coordinates.longitude)"
        }
    }
    
    var request: URLRequest {
        let url = URL(string: path, relativeTo: baseURL)
        
        return URLRequest(url: url!)
    }
    
    case Current(apiKey: String, coordinates: Coordinates)
}

final class APIWeatherManager: APIManager {

    let sessionConfiguration: URLSessionConfiguration
    let apiKey: String
    
    lazy var session: URLSession = {
        return URLSession(configuration: self.sessionConfiguration)
    } ()
    
    init(sessionConfiguration: URLSessionConfiguration, apiKey: String) {
        self.sessionConfiguration = sessionConfiguration
        self.apiKey = apiKey
    }
    
    convenience init(apiKey: String) {
        self.init(sessionConfiguration: .default, apiKey: apiKey)
    }

    func fetchCurrentWeatherWith(coordinates: Coordinates, completionHandler: (APIResult<CurrentWeather>) -> Void) {
        let request = ForecastType.Current(apiKey: self.apiKey, coordinates: coordinates).request
        
        fetch(request: request, parse: { (json) -> CurrentWeather? in
            if let dictionary = json["currently"] as? [String: AnyObject] {
                return CurrentWeather(JSON: dictionary)
            }
            else {
                return nil
            }
        }, completionHandler: completionHandler)
    }
}

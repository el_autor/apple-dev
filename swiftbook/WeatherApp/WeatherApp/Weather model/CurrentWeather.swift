//
//  CurrentWeather.swift
//  WeatherApp
//
//  Created by Vlad on 13.08.2020.
//  Copyright © 2020 Vlad Krivozubov. All rights reserved.
//

import UIKit

struct CurrentWeather: JSONDecodable {
    let temperature: Double
    let apparentTemperature: Double
    let humidity: Double
    let pressure: Double
    let icon: UIImage
}

extension CurrentWeather: JSONDecodable {
    init?(JSON: [String : AnyObject]) {
        guard let temperature = JSON["temperature"] as? Double,
        let appearentWeather = JSON["appearentTemperature"] as? Double,
        let humidity = JSON["humidity"] as? Double,
        let pressure = JSON["pressure"] as? Double,
        let iconString = JSON["icon"] as? String else {
            return nil
        }
        
        let icon = WeatherIconManager(rawValue: iconString).image
        
        self.temperature = temperature
        self.apparentTemperature = appearentWeather
        self.humidity = humidity
        self.pressure = pressure
        self.icon = icon
    }
}

extension CurrentWeather {
    
    var pressureString: String {
        get {
            return "\(Int(pressure))mm"
        }
    }
    
    var humidityString: String {
        return "\(Int(humidity))%"
    }
    
    var temperatureString: String {
        return "\(Int(temperature))˚C"
    }
    
    var ApparentTemperatureString: String {
        return "\(Int(apparentTemperature))˚C"
    }
}

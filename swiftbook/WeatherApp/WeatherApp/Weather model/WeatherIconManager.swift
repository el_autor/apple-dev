//
//  WeatherIconManager.swift
//  WeatherApp
//
//  Created by Vlad on 13.08.2020.
//  Copyright © 2020 Vlad Krivozubov. All rights reserved.
//

import UIKit

enum WeatherIconManager: String {
    
    case ClearDay = "clear-day"
    case ClearNight = "clear-night"
    case Rain = "rain"
    case Snow = "snow"
    case Sleet = "sleet"
    case Wind = "wind"
    case Fog = "fog"
    case Cloudy = "cloudy"
    case UnpredictedIcon = "unpredicted-icon"
    
    init(rawValue: String) {
        switch rawValue {
        case "clear-day":
            self = .ClearDay
        case "clear-night":
            self = .ClearNight
        case "rain":
            self = .Rain
        case "snow":
            self = .Snow
        case "sleet":
            self = .Sleet
        case "wind":
            self = .Wind
        case "fog":
            self = .Fog
        case "cloudy":
            self = .Cloudy
        default:
            self = .UnpredictedIcon
        }
    }
    
}

extension WeatherIconManager {
    var image: UIImage {
        get {
            return UIImage(named: self.rawValue)!
        }
    }
}

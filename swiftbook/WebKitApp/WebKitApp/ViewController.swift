//
//  ViewController.swift
//  WebKitApp
//
//  Created by Vlad on 07.08.2020.
//  Copyright © 2020 Vlad Krivozubov. All rights reserved.
//

import UIKit
import WebKit

class ViewController: UIViewController {

    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var forwardButton: UIButton!
    @IBOutlet weak var urlInput: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        urlInput.delegate = self
        webView.navigationDelegate = self
        
        let homePage = "https://www.google.ru"
        backButton.isEnabled = false
        forwardButton.isEnabled = false
        
        let url = URL(string: homePage)
        let request = URLRequest(url: url!)
        
        urlInput.text = homePage
        
        webView.load(request)
        webView.allowsBackForwardNavigationGestures = true
    }

    @IBAction func backPage(_ sender: UIButton) {
        if webView.canGoBack {
            webView.goBack()
        }
    }
    
    @IBAction func forwardPage(_ sender: UIButton) {
        if webView.canGoForward {
            webView.goForward()
        }
    }
    
}

extension ViewController: UITextFieldDelegate, WKNavigationDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let url = URL(string: textField.text!)
        let request = URLRequest(url: url!)
        
        webView.load(request)
        urlInput.resignFirstResponder()
        return true
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        urlInput.text = webView.url?.absoluteString
        
        backButton.isEnabled = webView.canGoBack
        forwardButton.isEnabled = webView.canGoForward
    }
}

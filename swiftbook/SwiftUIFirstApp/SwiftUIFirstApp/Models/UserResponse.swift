//
//  UserResponse.swift
//  FirstSwiftUIProject
//
//  Created by Vlad on 27.07.2020.
//  Copyright © 2020 Vlad Krivozubov. All rights reserved.
//

import Foundation
import UIKit
import SwiftUI

struct UserResponse: Hashable, Codable, Identifiable {
    var id: Int
    var name: String
    var profileImage: String
    var email: String
    var likes: String
    var text: String
}

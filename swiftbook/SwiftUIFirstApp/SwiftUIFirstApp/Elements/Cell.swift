//
//  Cell.swift
//  FirstSwiftUIProject
//
//  Created by Vlad on 27.07.2020.
//  Copyright © 2020 Vlad Krivozubov. All rights reserved.
//

import SwiftUI

struct Cell: View {
    var data: UserResponse
    
    var body: some View {
        VStack(spacing: 16.0) {
            TopView(data: data)
            Text(data.text)
            .lineLimit(nil)
        }.padding(10)
    }
}

struct Cell_Previews: PreviewProvider {
    static var previews: some View {
        Cell(data: userResponse[0])
    }
}

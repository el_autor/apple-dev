//
//  ContentView.swift
//  SwiftUIFirstApp
//
//  Created by Vlad on 27.07.2020.
//  Copyright © 2020 Vlad Krivozubov. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        NavigationView {
            List(userResponse) { user in
                Cell(data: user)
            }
            .navigationBarTitle(Text("Header"))
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

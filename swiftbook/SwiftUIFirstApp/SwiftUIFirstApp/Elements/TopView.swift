//
//  TopView.swift
//  FirstSwiftUIProject
//
//  Created by Vlad on 27.07.2020.
//  Copyright © 2020 Vlad Krivozubov. All rights reserved.
//

import SwiftUI

struct TopView: View {
    var data: UserResponse
    
    var body: some View {
        HStack(spacing: 10.0) {
            Image(data.profileImage)
            .resizable()
            .frame(width: 70, height: 70)
            .clipShape(Circle())
            VStack(alignment: .leading, spacing: 4.0) {
                Text(data.name)
                .font(.title)
                HStack {
                    Text(data.email)
                    .font(.subheadline)
                    Spacer()
                    Image("like")
                    Text(data.likes)
                    .font(.subheadline)
                }
            }
        }
    }
}

struct TopView_Previews: PreviewProvider {
    static var previews: some View {
        TopView(data: userResponse[0])
    }
}

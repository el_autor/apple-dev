//
//  CommandNetworkService.swift
//  MVCApp
//
//  Created by Vlad on 23.08.2020.
//  Copyright © 2020 Vlad Krivozubov. All rights reserved.
//

import Foundation

class CommentNetworkService {
    private init() {}
    
    static func getComments(completion: @escaping(GetCommentResponse) -> ()) {
        guard let url = URL(string: "https://jsonplaceholder.typicode.com/posts/1/comments") else { return }
        
        NetworkService.shared.getData(url: url, completion: { (json) in
            do {
                let response = try GetCommentResponse(json: json)
                completion(response)
            }
            catch {
                print(error)
            }
        })
    }
}

//
//  CommentCell.swift
//  MVCApp
//
//  Created by Vlad on 10.08.2020.
//  Copyright © 2020 Vlad Krivozubov. All rights reserved.
//

import UIKit

class CommentCell: UITableViewCell {
    
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var textField: UITextView!
    
    func setup(with comment: Comment) {
        headerLabel.text = comment.name
        textField.text = comment.body
    }
}

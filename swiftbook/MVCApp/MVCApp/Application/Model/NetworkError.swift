//
//  NetworkError.swift
//  MVCApp
//
//  Created by Vlad on 23.08.2020.
//  Copyright © 2020 Vlad Krivozubov. All rights reserved.
//

import Foundation

enum NetworkError: Error {
    case failInternetError
    case noInternetConnection
}


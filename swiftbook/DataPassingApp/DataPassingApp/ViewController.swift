//
//  ViewController.swift
//  DataPassingApp
//
//  Created by Vlad on 08.08.2020.
//  Copyright © 2020 Vlad Krivozubov. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var loginField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    
    @IBAction func sendPressed(sender: UIButton) {
        performSegue(withIdentifier: "detailSegue", sender: nil)
    }
    
    @IBAction func unwindToMainScreen(segue: UIStoryboardSegue) {
        guard segue.identifier == "unwindSegue" else { return }
        guard let sourceViewController = segue.source as? UserViewController else { return }
        self.loginField.text = sourceViewController.login + "comeback!"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loginField.placeholder = "Login"
        passwordField.placeholder = "Password"
        passwordField.isSecureTextEntry = true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let destinationViewController = segue.destination as? UserViewController else { return }
        destinationViewController.modalPresentationStyle = .fullScreen
        destinationViewController.login = loginField.text
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }

}


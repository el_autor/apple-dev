//
//  UserViewController.swift
//  DataPassingApp
//
//  Created by Vlad on 08.08.2020.
//  Copyright © 2020 Vlad Krivozubov. All rights reserved.
//

import UIKit

class UserViewController: UIViewController {

    var login: String!
    @IBOutlet weak var userLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        userLabel.text = login
    }
    
    @IBAction func sendPressed(sender: UIButton) {
        performSegue(withIdentifier: "unwindSegue", sender: nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

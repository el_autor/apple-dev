//
//  ArcColorCell.swift
//  CollectionViewApp
//
//  Created by Vlad on 06.08.2020.
//  Copyright © 2020 Vlad Krivozubov. All rights reserved.
//

import UIKit

class ArcColorCell: UICollectionViewCell {
    
    @IBOutlet weak var image: UIImageView!
    
}

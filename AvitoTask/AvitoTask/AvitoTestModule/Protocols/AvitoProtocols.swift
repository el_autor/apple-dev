protocol AvitoViewInput: AnyObject {
    func setOptionsCollectionViewTitleLabelText(text: String)
    
    func setChooseButtonText(text: String)
    
    func reloadData()
    
    func endRefreshing()
    
    func showAlert(title: String, message: String)
}

protocol AvitoViewOutput: AnyObject {
    func didLoadView()
    
    func didChooseOption(index: Int)
    
    func didTapChooseButton()
    
    func didRefreshRequested()
    
    func viewModel(for index: Int) -> Option?
    
    func getOptionsTotal() -> Int
}

protocol AvitoInteractorInput: AnyObject {
    func fetchData()
}

protocol AvitoInteractorOutput: AnyObject {
    func updateModel(newModel: AvitoPresenterData)
    
    func dataDidReceived()
    
    func showAlert(title: String, message: String)
}

protocol AvitoRouterInput: AnyObject {
    
}

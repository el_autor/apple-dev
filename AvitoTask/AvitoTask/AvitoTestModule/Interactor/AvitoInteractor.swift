final class AvitoInteractor {
    
    weak var output: AvitoInteractorOutput?
    
    private func convertToPresenterData(model: AvitoScreenDataRaw) -> AvitoPresenterData {
        let options: [Option] = model.list.filter { option in
            return option.isSelected
        }.map { option in
            return Option(title: option.title,
                          description: option.description,
                          icon: option.icon.size52x52,
                          price: option.price,
                          isChoosed: false)
        }
        
        return AvitoPresenterData(titleText: model.title,
                                  optionNotChoosenButtonText: model.actionTitle,
                                  optionChosenButtonText: model.selectedActionTitle,
                                  options: options)
    }
}

extension AvitoInteractor: AvitoInteractorInput {
    func fetchData() {
        NetworkService.shared.getAvitoScreenData { [weak self] (result) in
            if let error = result.error {
                self?.output?.showAlert(title: "Oops", message: error.type)
                return
            }
            
            guard let data = result.data,
                  let self = self else {
                return
            }
            
            self.output?.updateModel(newModel: self.convertToPresenterData(model: data))
            self.output?.dataDidReceived()
        }
    }
}

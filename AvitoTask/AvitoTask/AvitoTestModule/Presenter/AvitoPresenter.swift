final class AvitoPresenter {
    
    weak var view: AvitoViewInput?
    
    var interactor: AvitoInteractorInput?
    
    var router: AvitoRouterInput?
    
    private var model: AvitoPresenterData?
    
    private var checkedOptionIndex: Int?
}

extension AvitoPresenter: AvitoInteractorOutput {
    func updateModel(newModel: AvitoPresenterData) {
        self.model = newModel
    }
    
    func dataDidReceived() {
        guard let model = model else {
            return
        }
        
        view?.setOptionsCollectionViewTitleLabelText(text: model.titleText)
        view?.setChooseButtonText(text: model.optionNotChoosenButtonText)
        
        view?.reloadData()
        view?.endRefreshing()
    }
    
    func showAlert(title: String, message: String) {
        view?.showAlert(title: title, message: message)
    }
}

extension AvitoPresenter: AvitoViewOutput {
    func didLoadView() {
        interactor?.fetchData()
    }
    
    func didTapChooseButton() {
        guard let index = checkedOptionIndex else {
            return
        }
        
        view?.showAlert(title: model?.options[index].title ?? String(), message: String())
    }
    
    func didRefreshRequested() {
        checkedOptionIndex = nil
        interactor?.fetchData()
    }
    
    func didChooseOption(index: Int) {
        if let currentIndex = checkedOptionIndex {
            if currentIndex == index {
                model?.options[currentIndex].isChoosed = false
                checkedOptionIndex = nil
                view?.setChooseButtonText(text: model?.optionNotChoosenButtonText ?? String())
            } else {
                model?.options[currentIndex].isChoosed = false
                model?.options[index].isChoosed = true
                checkedOptionIndex = index
            }
        } else {
            checkedOptionIndex = index
            model?.options[index].isChoosed = true
            view?.setChooseButtonText(text: model?.optionChosenButtonText ?? String())
        }
        
        view?.reloadData()
    }
    
    func viewModel(for index: Int) -> Option? {
        return model?.options[index]
    }
    
    func getOptionsTotal() -> Int {
        return model?.options.count ?? .zero
    }
}

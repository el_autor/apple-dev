import Foundation
import UIKit

final class AvitoContainer {
    
    let viewController: AvitoViewController
    
    class func assemble(with: AvitoContext) -> AvitoContainer {
        let viewController = AvitoViewController()
        
        let presenter = AvitoPresenter()
        let interactor = AvitoInteractor()
        let router = AvitoRouter()
        
        viewController.output = presenter
        
        interactor.output = presenter
        
        router.viewController = viewController
        
        presenter.view = viewController
        presenter.router = router
        presenter.interactor = interactor
        
        return AvitoContainer(viewController: viewController)
    }
    
    private init(viewController: AvitoViewController) {
        self.viewController = viewController
    }
}

struct AvitoContext {
    
}

import UIKit

final class OptionCollectionViewCell: UICollectionViewCell {
    
    // MARK: Elements
    
    private weak var optionImageView: UIImageView!
    
    private weak var titleLabel: UILabel!
    
    weak var descriptionLabel: UILabel!
    
    private weak var priceLabel: UILabel!
    
    private weak var checkmakeImageView: UIImageView!
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        
        setupView()
        setupSubviews()
    }

    required init?(coder: NSCoder) {
        return nil
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        layoutOptionImageView()
        layoutTitleLabel()
        layoutDescriptionLabel()
        layoutPriceLabel()
        layoutCheckmarkImageView()
    }
    
    func configure(viewModel: Option) {
        checkmakeImageView.isHidden = !viewModel.isChoosed
        titleLabel.text = viewModel.title
        
        if let imageURL = URL(string: viewModel.icon) {
            optionImageView.downloadFromURL(url: imageURL)
        }
        
        descriptionLabel.text = viewModel.description ?? String()
        priceLabel.text = viewModel.price
    }
    
    private func setupView() {
        backgroundColor = Constants.backgroundColor
        layer.cornerRadius = Constants.cornerRadius
    }
    
    private func setupSubviews() {
        setupOptionImageView()
        setupTitleLabel()
        setupDescriptionLabel()
        setupPriceLabel()
        setupCheckmarkImageView()
    }
    
    private func setupOptionImageView() {
        let imageView = UIImageView()
        
        optionImageView = imageView
        addSubview(optionImageView)

        optionImageView.contentMode = .scaleToFill
        optionImageView.translatesAutoresizingMaskIntoConstraints = false
    }
    
    private func setupTitleLabel() {
        let label = UILabel()
        
        titleLabel = label
        addSubview(titleLabel)
        
        titleLabel.font = Constants.TitleLabel.font
        titleLabel.textColor = .black
        titleLabel.textAlignment = .left
        titleLabel.numberOfLines = .zero
        titleLabel.lineBreakMode = .byWordWrapping
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
    }
    
    private func setupDescriptionLabel() {
        let label = UILabel()
        
        descriptionLabel = label
        addSubview(descriptionLabel)
        
        descriptionLabel.font = Constants.DescriptionLabel.font
        descriptionLabel.textColor = .black
        descriptionLabel.textAlignment = .left
        descriptionLabel.numberOfLines = .zero
        descriptionLabel.lineBreakMode = .byWordWrapping
        descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
    }
    
    private func setupPriceLabel() {
        let label = UILabel()
        
        priceLabel = label
        addSubview(priceLabel)
        
        priceLabel.font = Constants.PriceLabel.font
        priceLabel.textColor = .black
        priceLabel.textAlignment = .left
        priceLabel.numberOfLines = .zero
        priceLabel.lineBreakMode = .byWordWrapping
        priceLabel.translatesAutoresizingMaskIntoConstraints = false
    }
    
    private func setupCheckmarkImageView() {
        let imageView = UIImageView()
        
        checkmakeImageView = imageView
        addSubview(checkmakeImageView)
        
        checkmakeImageView.image = UIImage(named: "checkmarkIcon")
        checkmakeImageView.contentMode = .scaleToFill
        checkmakeImageView.translatesAutoresizingMaskIntoConstraints = false
        checkmakeImageView.isHidden = true
    }
    
    private func layoutOptionImageView() {
        optionImageView.topAnchor.constraint(equalTo: topAnchor,
                                             constant: Constants.screenHeight * Constants.OptionImageView.topMarginMultiplier)
                                            .isActive = true
        optionImageView.leftAnchor.constraint(equalTo: leftAnchor,
                                              constant: Constants.screenWidth * Constants.OptionImageView.leftMarginMultiplier)
                                             .isActive = true
        optionImageView.widthAnchor.constraint(equalToConstant: Constants.OptionImageView.width).isActive = true
        optionImageView.heightAnchor.constraint(equalToConstant: Constants.OptionImageView.height).isActive = true
    }
    
    private func layoutTitleLabel() {
        titleLabel.topAnchor.constraint(equalTo: topAnchor,
                                        constant: Constants.screenHeight * Constants.TitleLabel.topMarginMultiplier)
                                       .isActive = true
        titleLabel.leftAnchor.constraint(equalTo: optionImageView.rightAnchor,
                                         constant: Constants.screenWidth * Constants.TitleLabel.leftMarginMultiplier)
                                        .isActive = true
        titleLabel.rightAnchor.constraint(equalTo: checkmakeImageView.leftAnchor,
                                          constant: -Constants.screenWidth * Constants.TitleLabel.rightMarginMultiplier)
                                         .isActive = true
    }
    
    private func layoutDescriptionLabel() {
        descriptionLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor,
                                              constant: Constants.screenHeight * Constants.DescriptionLabel.topMarginMultiplier)
                                             .isActive = true
        descriptionLabel.leftAnchor.constraint(equalTo: optionImageView.rightAnchor,
                                               constant: Constants.screenWidth * Constants.DescriptionLabel.leftMarginMultiplier)
                                              .isActive = true
        descriptionLabel.rightAnchor.constraint(equalTo: checkmakeImageView.leftAnchor,
                                                constant: -Constants.screenWidth * Constants.DescriptionLabel.rightMarginMultiplier)
                                               .isActive = true
    }
    
    private func layoutPriceLabel() {
        priceLabel.bottomAnchor.constraint(equalTo: bottomAnchor,
                                           constant: -Constants.screenHeight * Constants.PriceLabel.bottomMarginMultiplier)
                                          .isActive = true
        priceLabel.leftAnchor.constraint(equalTo: optionImageView.rightAnchor,
                                         constant: Constants.screenWidth * Constants.PriceLabel.leftMarginMultiplier)
                                        .isActive = true
        priceLabel.rightAnchor.constraint(equalTo: checkmakeImageView.leftAnchor,
                                          constant: -Constants.screenWidth * Constants.PriceLabel.rightMarginMultiplier)
                                         .isActive = true
    }
    
    private func layoutCheckmarkImageView() {
        checkmakeImageView.centerYAnchor.constraint(equalTo: optionImageView.centerYAnchor).isActive = true
        checkmakeImageView.rightAnchor.constraint(equalTo: rightAnchor,
                                                  constant: -Constants.screenWidth * Constants.CheckmarkImageView.rightMarginMultiplier)
                                                 .isActive = true
        checkmakeImageView.widthAnchor.constraint(equalTo: widthAnchor,
                                                  multiplier: Constants.CheckmarkImageView.widthMultiplier)
                                                 .isActive = true
        checkmakeImageView.heightAnchor.constraint(equalTo: checkmakeImageView.widthAnchor).isActive = true
    }

}

extension OptionCollectionViewCell {
    internal struct Constants {
        static let screenWidth: CGFloat = UIScreen.main.bounds.width
        
        static let screenHeight: CGFloat = UIScreen.main.bounds.height
        
        static let backgroundColor: UIColor = UIColor(red: 245 / 255,
                                                      green: 245 / 255,
                                                      blue: 245 / 255,
                                                      alpha: 1)
        
        static let cornerRadius: CGFloat = 5
        
        struct OptionImageView {
            static let topMarginMultiplier: CGFloat = 0.02
            
            static let leftMarginMultiplier: CGFloat = 0.05
            
            static let width: CGFloat = 52
            
            static let height: CGFloat = 52
        }
        
        struct TitleLabel {
            static let font: UIFont? = UIFont.boldSystemFont(ofSize: 20)
            
            static let topMarginMultiplier: CGFloat = 0.03
            
            static let leftMarginMultiplier: CGFloat = 0.05
            
            static let rightMarginMultiplier: CGFloat = 0.03
        }
        
        struct CheckmarkImageView {
            static let widthMultiplier: CGFloat = 0.075
            
            static let rightMarginMultiplier: CGFloat = 0.05
        }
        
        struct DescriptionLabel {
            static let font: UIFont? = UIFont.systemFont(ofSize: 12)
            
            static let topMarginMultiplier: CGFloat = 0.02
            
            static let leftMarginMultiplier: CGFloat = 0.05
            
            static let rightMarginMultiplier: CGFloat = 0.03
            
            static let bottomMarginMultiplier: CGFloat = 0.02
        }
        
        struct PriceLabel {
            static let font: UIFont? = UIFont.boldSystemFont(ofSize: 17)
            
            static let bottomMarginMultiplier: CGFloat = 0.03
            
            static let leftMarginMultiplier: CGFloat = 0.05
            
            static let rightMarginMultiplier: CGFloat = 0.03
        }
    }
}


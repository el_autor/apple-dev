import UIKit

final class AvitoViewController: UIViewController {

    var output: AvitoViewOutput?
    
    // MARK: Elements
    
    private weak var closeImageView: UIImageView!
    
    private weak var optionsCollectionViewTitleLabel: UILabel!
    
    private weak var optionsCollectionView: UICollectionView!
    
    // MARK: Labels for cell adaptive layout
    
    private var titleTestLabel: UILabel!
    
    private var descriptionTestLabel: UILabel!
    
    private var priceTestLabel: UILabel!
    
    private weak var chooseButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        setupSubviews()
        setupTestLabels()
        
        output?.didLoadView()
    }
    
    override func viewDidLayoutSubviews() {
        layoutCloseImageView()
        layoutOptionsCollectionViewTitleLabel()
        layoutOptionsCollectionView()
        layoutChooseButton()
    }
    
    private func setupView() {
        view.backgroundColor = .white
    }
    
    private func setupSubviews() {
        setupCloseImageView()
        setupOptionsCollectionViewTitleLabel()
        setupOptionsCollectionView()
        setupChooseButton()
    }
    
    private func setupTestLabels() {
        descriptionTestLabel = UILabel()
        
        descriptionTestLabel.font = OptionCollectionViewCell.Constants.DescriptionLabel.font
        descriptionTestLabel.textColor = .black
        descriptionTestLabel.textAlignment = .left
        descriptionTestLabel.numberOfLines = .zero
        descriptionTestLabel.lineBreakMode = .byWordWrapping
        descriptionTestLabel.translatesAutoresizingMaskIntoConstraints = false
        
        titleTestLabel = UILabel()
        
        titleTestLabel.font = OptionCollectionViewCell.Constants.TitleLabel.font
        titleTestLabel.textColor = .black
        titleTestLabel.textAlignment = .left
        titleTestLabel.numberOfLines = .zero
        titleTestLabel.lineBreakMode = .byWordWrapping
        titleTestLabel.translatesAutoresizingMaskIntoConstraints = false
        
        priceTestLabel = UILabel()
        
        priceTestLabel.font = OptionCollectionViewCell.Constants.PriceLabel.font
        priceTestLabel.textColor = .black
        priceTestLabel.textAlignment = .left
        priceTestLabel.numberOfLines = .zero
        priceTestLabel.lineBreakMode = .byWordWrapping
        priceTestLabel.translatesAutoresizingMaskIntoConstraints = false
    }
    
    private func setupCloseImageView() {
        let imageView = UIImageView()
        
        closeImageView = imageView
        view.addSubview(closeImageView)

        closeImageView.image = UIImage(named: Constants.CancelImageView.imageName)
        closeImageView.contentMode = .scaleToFill
        
        closeImageView.translatesAutoresizingMaskIntoConstraints = false
    }
    
    private func setupOptionsCollectionViewTitleLabel() {
        let label = UILabel()
        
        optionsCollectionViewTitleLabel = label
        view.addSubview(optionsCollectionViewTitleLabel)
        
        optionsCollectionViewTitleLabel.lineBreakMode = .byWordWrapping
        optionsCollectionViewTitleLabel.numberOfLines = .zero
        optionsCollectionViewTitleLabel.font = Constants.OptionsCollectionViewTitleLabel.font
        optionsCollectionViewTitleLabel.textColor = .black
        optionsCollectionViewTitleLabel.textAlignment = .left
        
        optionsCollectionViewTitleLabel.translatesAutoresizingMaskIntoConstraints = false
    }
    
    private func setupOptionsCollectionView() {
        let flowLayout = UICollectionViewFlowLayout()
        
        flowLayout.scrollDirection = .vertical
        flowLayout.minimumInteritemSpacing = Constants.OptionsCollectionView.internalSpacing
    
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: flowLayout)
        
        optionsCollectionView = collectionView
        view.addSubview(collectionView)
        
        optionsCollectionView.delegate = self
        optionsCollectionView.dataSource = self
        
        optionsCollectionView.register(OptionCollectionViewCell.self,
                                       forCellWithReuseIdentifier: Constants.OptionsCollectionView.cellIdentifier)
        
        optionsCollectionView.showsVerticalScrollIndicator = false
        optionsCollectionView.backgroundColor = .white
        optionsCollectionView.translatesAutoresizingMaskIntoConstraints = false
        
        let refreshControl = UIRefreshControl()
        
        refreshControl.addTarget(self, action: #selector(didRequestRefresh), for: .valueChanged)
        optionsCollectionView.refreshControl = refreshControl
    }
    
    private func setupChooseButton() {
        let button = UIButton()
        
        chooseButton = button
        view.addSubview(chooseButton)
        
        chooseButton.backgroundColor = Constants.ChooseButton.backgroundColor
        chooseButton.layer.cornerRadius = Constants.ChooseButton.cornerRadius
        chooseButton.setTitleColor(.white, for: .normal)
        
        chooseButton.addTarget(self, action: #selector(didTapChooseButton), for: .touchUpInside)
        
        chooseButton.translatesAutoresizingMaskIntoConstraints = false
    }
    
    private func layoutCloseImageView() {
        closeImageView.topAnchor.constraint(equalTo: view.topAnchor,
                                            constant: Constants.screenHeight * Constants.CancelImageView.topMarginMupltiplier)
                                            .isActive = true
        closeImageView.leftAnchor.constraint(equalTo: view.leftAnchor,
                                             constant: Constants.screenWidth * Constants.CancelImageView.leftMarginMultiplier)
                                            .isActive = true
        closeImageView.widthAnchor.constraint(equalTo: view.widthAnchor,
                                              multiplier: Constants.CancelImageView.widthMultiplier)
                                             .isActive = true
        closeImageView.heightAnchor.constraint(equalTo: closeImageView.widthAnchor).isActive = true
    }
    
    private func layoutOptionsCollectionViewTitleLabel() {
        optionsCollectionViewTitleLabel.topAnchor.constraint(equalTo: closeImageView.bottomAnchor,
                                                             constant: Constants.screenHeight * Constants.OptionsCollectionViewTitleLabel.topMarginMupltiplier)
                                                            .isActive = true
        optionsCollectionViewTitleLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        optionsCollectionViewTitleLabel.widthAnchor.constraint(equalTo: view.widthAnchor,
                                                               multiplier: Constants.OptionsCollectionViewTitleLabel.widthMultiplier)
                                                              .isActive = true
    }
    
    private func layoutOptionsCollectionView() {
        optionsCollectionView.topAnchor.constraint(equalTo: optionsCollectionViewTitleLabel.bottomAnchor,
                                                   constant: Constants.screenHeight * Constants.OptionsCollectionView.topMarginMultiplier)
                                                  .isActive = true
        optionsCollectionView.centerXAnchor.constraint(equalTo: view.centerXAnchor)
                                                     .isActive = true
        optionsCollectionView.widthAnchor.constraint(equalTo: view.widthAnchor,
                                                     multiplier: Constants.OptionsCollectionView.widthMultiplier)
                                                   .isActive = true
        optionsCollectionView.bottomAnchor.constraint(equalTo: chooseButton.topAnchor,
                                                      constant: -Constants.screenHeight * Constants.OptionsCollectionView.bottomMarginMultiplier)
                                                     .isActive = true
    }
    
    private func layoutChooseButton() {
        chooseButton.bottomAnchor.constraint(equalTo: view.bottomAnchor,
                                             constant: -Constants.screenHeight * Constants.ChooseButton.bottomMarginMultiplier)
                                            .isActive = true
        chooseButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        chooseButton.widthAnchor.constraint(equalTo: view.widthAnchor,
                                            multiplier: Constants.ChooseButton.widthMultiplier)
                                           .isActive = true
        
        if Constants.screenHeight * Constants.ChooseButton.heightMultilier < 44 {
            chooseButton.heightAnchor.constraint(equalToConstant: 44).isActive = true
        } else {
            chooseButton.heightAnchor.constraint(equalTo: view.heightAnchor,
                                                 multiplier: Constants.ChooseButton.heightMultilier)
                                                .isActive = true
        }
    }
    
    @objc
    private func didTapChooseButton() {
        output?.didTapChooseButton()
    }
    
    @objc
    private func didRequestRefresh() {
        output?.didRefreshRequested()
    }
}

extension AvitoViewController: AvitoViewInput {
    func setOptionsCollectionViewTitleLabelText(text: String) {
        optionsCollectionViewTitleLabel.text = text
    }
    
    func setChooseButtonText(text: String) {
        chooseButton.setTitle(text, for: .normal)
    }
    
    func reloadData() {
        optionsCollectionView.reloadData()
    }
    
    func endRefreshing() {
        optionsCollectionView.refreshControl?.endRefreshing()
    }
    
    func showAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "ОК", style: .default, handler: nil)

        alert.addAction(okAction)

        present(alert, animated: true, completion: nil)
    }
}

extension AvitoViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        output?.didChooseOption(index: indexPath.row)
    }
}

extension AvitoViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.OptionsCollectionView.cellIdentifier,
                                                            for: indexPath) as? OptionCollectionViewCell else {
            return UICollectionViewCell()
        }
        
        guard let optionModel = output?.viewModel(for: indexPath.row) else {
            return cell
        }
        
        cell.configure(viewModel: optionModel)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return output?.getOptionsTotal() ?? .zero
    }
}

extension AvitoViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        guard let viewModel = output?.viewModel(for: indexPath.row) else {
            return CGSize(width: CGFloat.zero, height: CGFloat.zero)
        }
        
        descriptionTestLabel.text = viewModel.description ?? String()
        titleTestLabel.text = viewModel.title
        priceTestLabel.text = viewModel.price
        
        let labelsWidth = optionsCollectionView.bounds.width -
            Constants.screenWidth *
            (OptionCollectionViewCell.Constants.OptionImageView.leftMarginMultiplier +
                OptionCollectionViewCell.Constants.TitleLabel.leftMarginMultiplier +
                OptionCollectionViewCell.Constants.TitleLabel.rightMarginMultiplier +
                OptionCollectionViewCell.Constants.CheckmarkImageView.rightMarginMultiplier) -
            OptionCollectionViewCell.Constants.OptionImageView.width -
            optionsCollectionView.bounds.width * OptionCollectionViewCell.Constants.CheckmarkImageView.widthMultiplier
        
        
        var offsetMultiplier: CGFloat = OptionCollectionViewCell.Constants.TitleLabel.topMarginMultiplier +
            OptionCollectionViewCell.Constants.PriceLabel.bottomMarginMultiplier +
            OptionCollectionViewCell.Constants.DescriptionLabel.bottomMarginMultiplier
        
        
        if !(descriptionTestLabel.sizeThatFits(CGSize(width: labelsWidth,
                                                    height: .zero)).height == .zero) {
            offsetMultiplier += OptionCollectionViewCell.Constants.DescriptionLabel.topMarginMultiplier
        }
        
        return CGSize(width: optionsCollectionView.bounds.width,
                      height: descriptionTestLabel.sizeThatFits(CGSize(width: labelsWidth, height: .zero)).height +
                        titleTestLabel.sizeThatFits(CGSize(width: labelsWidth, height: .zero)).height +
                        Constants.screenHeight * offsetMultiplier +
                        priceTestLabel.sizeThatFits(CGSize(width: labelsWidth, height: .zero)).height)
    }
}

extension AvitoViewController {
    private struct Constants {
        
        static let screenWidth = UIScreen.main.bounds.width
        
        static let screenHeight = UIScreen.main.bounds.height
        
        struct CancelImageView {
            static let imageName: String = "closeIcon"
            
            static let widthMultiplier: CGFloat = 0.09
            
            static let topMarginMupltiplier: CGFloat = 0.04
            
            static let leftMarginMultiplier: CGFloat = 0.04
        }
        
        struct OptionsCollectionViewTitleLabel {
            static let font: UIFont = UIFont.boldSystemFont(ofSize: 26)
            
            static let internalSpacing: CGFloat = 5
            
            static let widthMultiplier: CGFloat = 0.9
            
            static let heightMultiplier: CGFloat = 0.3
            
            static let topMarginMupltiplier: CGFloat = 0.1
        }
        
        struct OptionsCollectionView {
            static let cellIdentifier: String = "OptionCell"
            
            static let internalSpacing: CGFloat = 5
            
            static let topMarginMultiplier: CGFloat = 0.05
            
            static let widthMultiplier: CGFloat = 0.9
            
            static let bottomMarginMultiplier: CGFloat = 0.05
        }
        
        struct ChooseButton {
            static let backgroundColor: UIColor = .systemBlue
            
            static let cornerRadius: CGFloat = 5
            
            static let heightMultilier: CGFloat = 0.06
            
            static let bottomMarginMultiplier: CGFloat = 0.03
            
            static let widthMultiplier: CGFloat = 0.9
        }
    }
}

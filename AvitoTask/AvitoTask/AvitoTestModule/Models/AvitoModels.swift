struct AvitoPresenterData {
    var titleText: String
    
    var optionNotChoosenButtonText: String
    
    var optionChosenButtonText: String
    
    var options: [Option]
}


struct Option {
    var title: String
    
    var description: String?
    
    var icon: String
    
    var price: String
    
    var isChoosed: Bool
}

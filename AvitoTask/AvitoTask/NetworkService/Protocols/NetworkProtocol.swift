protocol NetworkServiceInput {
    func getAvitoScreenData(completion: @escaping (Result<AvitoScreenDataRaw, NetworkError>) -> Void)
}

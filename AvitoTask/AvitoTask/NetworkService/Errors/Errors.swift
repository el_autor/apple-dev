enum NetworkError: Error {
    case networkNotReachable
    
    case connectionToServerError
    
    case serializationError
    
    case unknownError
}

extension NetworkError {
    var type: String {
        switch self {
        case .networkNotReachable:
            return ErrorDescription.networkNotReachable
        case .connectionToServerError:
            return ErrorDescription.connectionToServerError
        case .serializationError:
            return ErrorDescription.serializationError
        case .unknownError:
            return ErrorDescription.unknownError
        }
    }
}

extension NetworkError {
    private struct ErrorDescription {
        static let networkNotReachable: String = "User has no network"

        static let connectionToServerError: String = "Server not reachable"
        
        static let serializationError: String = "Can't decode response"

        static let unknownError: String = "UnknownError"
    }
}

struct Result<DataType, ErrorType> {
    var data: DataType?
    
    var error: ErrorType?
}

import Foundation

final class NetworkService {
    
    static let shared: NetworkService = NetworkService()
    
    private init() {
        
    }
}

extension NetworkService: NetworkServiceInput {
    
    // MARK: Имитация запроса
    
    func getAvitoScreenData(completion: @escaping (Result<AvitoScreenDataRaw, NetworkError>) -> Void) {
        var result = Result<AvitoScreenDataRaw, NetworkError>()
        
        guard let jsonPath = Bundle.main.path(forResource: Constants.dataFilename, ofType: "json"),
              let jsonData = NSData(contentsOfFile: jsonPath) as Data? else {
            result.error = NetworkError.connectionToServerError
            completion(result)
            return
        }
        
        do {
            let response = try JSONDecoder().decode(AvitoScreenResponseRaw.self,
                                                    from: jsonData)
            
            guard response.status == Constants.success else {
                result.error = NetworkError.unknownError
                completion(result)
                return
            }
            
            result.data = response.result
            completion(result)
        } catch {
            result.error = NetworkError.serializationError
            completion(result)
        }
    }
}

extension NetworkService {
    private struct Constants {
        static let dataFilename: String = "result"
        
        static let statusKey: String = "status"
        
        static let resultKey: String = "data"
        
        static let success: String = "ok"
    }
}

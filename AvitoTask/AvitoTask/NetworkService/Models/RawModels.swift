struct AvitoScreenResponseRaw: Decodable {
    let status: String
    
    let result: AvitoScreenDataRaw?
}

struct AvitoScreenDataRaw: Decodable {
    let title: String
    
    let actionTitle: String
    
    let selectedActionTitle: String
    
    let list: [OptionRaw]
}

struct OptionRaw: Decodable {
    let id: String
    
    let title: String
    
    let description: String?
    
    let icon: IconRaw
    
    let price: String
    
    let isSelected: Bool
}

struct IconRaw: Decodable {
    let size52x52: String
    
    private enum CodingKeys: String, CodingKey {
        case size52x52 = "52x52"
    }
}

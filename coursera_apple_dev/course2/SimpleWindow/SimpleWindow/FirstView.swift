//
//  FirstView.swift
//  SimpleWindow
//
//  Created by Vlad on 16.09.2020.
//  Copyright © 2020 Vlad Krivozubov. All rights reserved.
//

import UIKit

class FirstView: UIViewController {
    
    @IBOutlet weak var launchTimeLabel: UILabel!
    @IBOutlet weak var launchTiming: UILabel!
    
    @IBOutlet weak var appearenceTimeLabel: UILabel!
    @IBOutlet weak var appearenceTime: UILabel!
    
    @IBOutlet weak var pushButton: UIButton!
    
    var dateFormatter: DateFormatter!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Simple App"
        
        let barButton = UIBarButtonItem(title: "Update", style: .done, target: self, action: #selector(updateThroughBar))
        barButton.setTitleTextAttributes([
            NSAttributedString.Key.font : UIFont.systemFont(ofSize: 17)],
                for: .normal)
        navigationItem.rightBarButtonItem = barButton
          //  = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(updateThroughBar))

        self.dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .medium
        
        self.launchTiming.text = dateFormatter.string(from: Date())
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.appearenceTime.text = dateFormatter.string(from: Date())
    }

    @IBAction func changeView(_ sender: Any) {
        self.navigationController?.pushViewController(SecondView(), animated: true)
    }
    
    @objc func updateThroughBar(_ sender: UIBarButtonItem!) {
        self.appearenceTime.text = dateFormatter.string(from: Date())
    }
}

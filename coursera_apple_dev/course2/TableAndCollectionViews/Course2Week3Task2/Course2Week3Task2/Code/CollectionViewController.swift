//
//  CollectionViewController.swift
//  Course2Week3Task2
//
//  Copyright © 2018 e-Legion. All rights reserved.
//

import UIKit

class CollectionViewController: UIViewController, UICollectionViewDataSource, CustomFlowLayoutDelegate {
    
    @IBOutlet weak var photosCollectionView: UICollectionView!
    
    var photos: [Photo] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        photos = PhotoProvider().photos()
        
        if let customLayout = photosCollectionView.collectionViewLayout as? CustomFlowLayout {
            customLayout.delegate = self
        }
        
        photosCollectionView.dataSource = self
        photosCollectionView.register(cellType: PhotoCollectionViewCell.self)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        photosCollectionView.frame = view.frame
    }
    
    // MARK: UICollectionViewDataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueCell(of: PhotoCollectionViewCell.self, to: indexPath) as PhotoCollectionViewCell
    
        cell.configure(with: photos[indexPath.row])
        
        return cell
    }
    
    // MARK: CustomFlowLayoutDelegate
    
    func collectionView(_ collectionView: UICollectionView, heightForPhotoAtIndexPath indexPath: IndexPath) -> CGFloat {
        if indexPath.item == photos.startIndex {
            return 300
        }
        else {
            return 200
        }
    }
}

extension UICollectionView {
    func register<Cell: UICollectionViewCell>(cellType: Cell.Type, nib: Bool = true) {
        
        let reuseIdentifier = String(describing: cellType)
        
        //print(reuseIdentifier)
        //PhotoCollectionViewCell
        
        if nib {
            let nib = UINib(nibName: reuseIdentifier, bundle: nil)
            register(nib, forCellWithReuseIdentifier: reuseIdentifier)
        }
        else {
            register(cellType, forCellWithReuseIdentifier: reuseIdentifier)
        }
    }
    
    func dequeueCell<Cell: UICollectionViewCell>(of cellType: Cell.Type, to indexPath: IndexPath) -> Cell {
        let cell = dequeueReusableCell(withReuseIdentifier: String(describing: cellType), for: indexPath) as! Cell
        
        return cell
    }
}

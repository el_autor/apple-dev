//
//  TableViewController.swift
//  Course2Week3Task2
//
//  Copyright © 2018 e-Legion. All rights reserved.
//

import UIKit

class TableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var photoTableView: UITableView!
    
    var photos: [Photo] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let provider = PhotoProvider()
        photos = provider.photos()
        
        /*
        photosTableView.separatorStyle = .singleLine
        photosTableView.separatorInset = UIEdgeInsets(top: 0, left: 5.0, bottom: 0, right: 3.0)
        photosTableView.separatorInsetReference = .fromCellEdges
        */
        
        photoTableView.register(UITableViewCell.self, forCellReuseIdentifier: "DefaultCell")
        
        photoTableView.dataSource = self
        photoTableView.delegate = self
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        photoTableView.frame = view.frame
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return photos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let photoCell = photoTableView.dequeueReusableCell(withIdentifier: "DefaultCell", for: indexPath)
        
        photoCell.textLabel!.text = photos[indexPath.row].name
        photoCell.imageView!.image = photos[indexPath.row].image
        
        let detailButton = UIButton(type: .detailDisclosure)
        detailButton.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
        
        photoCell.accessoryView = detailButton as UIView
        
        return photoCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        photoTableView.deselectRow(at: indexPath, animated: true)
        print("Row selected")
    }
    
    @objc func buttonTapped(_ sender: UIButton) {
        print("Accessory selected!")
    }
}

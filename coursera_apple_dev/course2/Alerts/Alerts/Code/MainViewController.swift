//
//  MainViewController.swift
//  Alerts
//
//  Copyright © 2018 e-Legion. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {

    @IBAction func showAlertButtonPressed(_ sender: UIButton) {
        let alertController = UIAlertController(title: "This is alert!",
                                      message: "Optionaly explain user the reason why this allert appears.",
                                      preferredStyle: .alert)
        
        let positiveAction = UIAlertAction(title: "Do it!", style: .default) {
            [weak alertController] _ in
            
            print("Do it pressed")
            print("Text field value: \(alertController?.textFields?[0].text ?? "Empty")")
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) {
            _ in
            
            print("Cancel pressed")
        }
        
        alertController.addAction(positiveAction)
        alertController.addAction(cancelAction)
        
        alertController.addTextField {
            textField in
            
            textField.backgroundColor = .green
        }
        
        alertController.preferredAction = positiveAction
        
        present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func showActionSheetButtonPressed(_ sender: UIButton) {
        let alertController = UIAlertController(title: "This is action sheet!",
                                                message: "Optionaly explain user the reason why this action sheet appears.",
                                                preferredStyle: .actionSheet)
        
        let firstOption = UIAlertAction(title: "Do something", style: .default) {
            _ in
            
            print("Do something pressed")
        }
        let secondOption = UIAlertAction(title: "Do something else", style: .default) {
            _ in
            
            print("Do something else pressed")
        }
        let deleteOption = UIAlertAction(title: "Delete everything", style: .destructive) {
            _ in
            
            print("Delete pressed")
        }
        let cancelOption = UIAlertAction(title: "Cancel", style: .cancel) {
            _ in
            
            print("Cancel pressed")
        }
        
        alertController.addAction(firstOption)
        alertController.addAction(secondOption)
        alertController.addAction(deleteOption)
        alertController.addAction(cancelOption)
        
        present(alertController, animated: true)
    }
}

//
//  ViewController5.swift
//  HierachyApp
//
//  Created by Vlad on 16.09.2020.
//  Copyright © 2020 Vlad Krivozubov. All rights reserved.
//

import UIKit

class ViewController5: UIViewController {
    private lazy var backButton: UIButton = {
        var button = UIButton(type: .system)
        button.setTitle("Back", for: .normal)
        button.setTitleColor(.blue, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        button.addTarget(self, action: #selector(backVC), for: .touchUpInside)
        return button
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .gray
        view.addSubview(backButton)
        bottonConctr()
    }
    
    @objc private func backVC(parametrSender: Any) {
        tabBarController?.tabBar.isHidden = false
        willMove(toParent: nil)
        view.removeFromSuperview()
        removeFromParent()
    }
    
    private func bottonConctr() {
        backButton.translatesAutoresizingMaskIntoConstraints = false
        backButton.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 0).isActive = true
        backButton.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: 0).isActive = true
        backButton.widthAnchor.constraint(equalToConstant: 50).isActive = true
    }
    
  
}

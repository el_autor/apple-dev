//
//  ViewController3.swift
//  HierachyApp
//
//  Created by Vlad on 16.09.2020.
//  Copyright © 2020 Vlad Krivozubov. All rights reserved.
//

import UIKit

class ViewController3: UIViewController {
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var unwindButton: UIButton!
    
    var text: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        textLabel.text = self.text
        // Do any additional setup after loading the view.
    }
    

    @IBAction func backTo2(_ sender: Any) {
        performSegue(withIdentifier: "unwindTo2", sender: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

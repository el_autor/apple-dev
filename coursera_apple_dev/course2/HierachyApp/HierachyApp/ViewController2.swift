//
//  ViewController2.swift
//  HierachyApp
//
//  Created by Vlad on 16.09.2020.
//  Copyright © 2020 Vlad Krivozubov. All rights reserved.
//

import UIKit

class ViewController2: UIViewController {
    @IBOutlet weak var goTo4: UIButton!
    @IBOutlet weak var addChild: UIButton!
    private let childVC = ViewController5()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func unwindTo3VC(segue: UIStoryboardSegue) {
        
    }
    
    @IBAction func goToNib(_ sender: Any) {
        let viewController4 = ViewController4()
        viewController4.modalPresentationStyle = .fullScreen
        present(viewController4, animated: true, completion: nil)
    }
    
    @IBAction func goTo5(_ sender: Any) {
        tabBarController?.tabBar.isHidden = true
        addChild(childVC)
        view.addSubview(childVC.view)
        childVC.didMove(toParent: self)
        childVC.view.translatesAutoresizingMaskIntoConstraints = false
        childConstraint()
    }
    
    private func childConstraint() {
        childVC.view.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
        childVC.view.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
        childVC.view.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        childVC.view.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    }

}

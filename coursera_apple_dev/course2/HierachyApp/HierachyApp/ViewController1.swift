//
//  ViewController1.swift
//  HierachyApp
//
//  Created by Vlad on 16.09.2020.
//  Copyright © 2020 Vlad Krivozubov. All rights reserved.
//

import UIKit

class ViewController1: UIViewController {
    @IBOutlet weak var entry: UITextField!
    @IBOutlet weak var to3VC: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func goTo3(_ sender: UIButton) {
        performSegue(withIdentifier: "2to3", sender: nil)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? ViewController3 {
            destination.text = entry.text!
        }
    }

}

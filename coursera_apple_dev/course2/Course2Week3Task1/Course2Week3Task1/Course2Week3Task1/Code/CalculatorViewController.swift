//
//  CalculatorViewController.swift
//  Course2Week3Task1
//
//  Copyright © 2018 e-Legion. All rights reserved.
//

import UIKit

class CalculatorViewController: UIViewController {

    @IBOutlet weak var resultLabel: UILabel!
    @IBOutlet weak var resultTopConstarint: NSLayoutConstraint!
    @IBOutlet weak var calcButton: UIButton!
    @IBOutlet weak var firstOperandLabel: UILabel!
    @IBOutlet weak var firstOperandValue: UILabel!
    @IBOutlet weak var stepper: UIStepper!
    @IBOutlet weak var secondOperandLabel: UILabel!
    @IBOutlet weak var secondOperandValue: UILabel!
    @IBOutlet weak var slider: UISlider!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor(red: 45 / 255,
                                            green: 47 / 255,
                                            blue: 49 / 255,
                                            alpha: 1)
        resultLabel.backgroundColor = UIColor(red: 238 / 255,
                                              green: 238 / 255,
                                              blue: 238 / 255,
                                              alpha: 1)
        resultLabel.textAlignment = .right
        resultLabel.font = UIFont.systemFont(ofSize: 30)
        resultLabel.textColor = UIColor(red: 0,
                                        green: 0,
                                        blue: 0,
                                        alpha: 1)
        resultLabel.text = "1"
        
        resultTopConstarint.constant = 32 - UIApplication.shared.statusBarFrame.size.height
        
        calcButton.backgroundColor = UIColor(red: 236 / 255,
                                             green: 113 / 255,
                                             blue: 73 / 255,
                                             alpha: 1)
        calcButton.titleLabel?.font = UIFont.systemFont(ofSize: 17)
        calcButton.setTitleColor(UIColor(red: 1,
                                         green: 1,
                                         blue: 1,
                                         alpha: 1),
                                 for: .normal)
        calcButton.setTitle("Calculate", for: .normal)
        
        firstOperandLabel.text = "First operand"
        firstOperandLabel.font = UIFont.systemFont(ofSize: 17)
        firstOperandLabel.textColor = UIColor(red: 1,
                                              green: 1,
                                              blue: 1,
                                              alpha: 1)
        
        firstOperandValue.text = "1.0000"
        firstOperandValue.font = UIFont.systemFont(ofSize: 17)
        firstOperandValue.textColor = UIColor(red: 1,
                                              green: 1,
                                              blue: 1,
                                              alpha: 1)
        
        stepper.setDecrementImage(stepper.decrementImage(for: .normal), for: .normal)
        stepper.setIncrementImage(stepper.incrementImage(for: .normal), for: .normal)
        stepper.value = 1
        stepper.minimumValue = 1
        stepper.maximumValue = 10
        stepper.stepValue = 0.5
        stepper.layer.cornerRadius = 5
        stepper.tintColor = UIColor(red: 236 / 255,
                                          green: 113 / 255,
                                          blue: 73 / 255,
                                          alpha: 1)
        // Здесь баг для IOS 13- заданный цвет stepper не отображается(tint color частично исправил за счет строк 66, 67)
        stepper.backgroundColor = self.view.backgroundColor
        
        secondOperandLabel.text = "Second operand"
        secondOperandLabel.font = UIFont.systemFont(ofSize: 17)
        secondOperandLabel.textColor = UIColor(red: 1,
                                              green: 1,
                                              blue: 1,
                                              alpha: 1)
        
        secondOperandValue.text = "1.0000"
        secondOperandValue.font = UIFont.systemFont(ofSize: 17)
        secondOperandValue.textColor = UIColor(red: 1,
                                              green: 1,
                                              blue: 1,
                                              alpha: 1)
        
        slider.value = 1
        slider.minimumValue = 1
        slider.maximumValue = 100
        slider.tintColor = UIColor(red: 236 / 255,
                                   green: 113 / 255,
                                   blue: 73 / 255,
                                   alpha: 1)
    }
    
    @IBAction func changeFirstOperand(_ sender: UIStepper) {
        let formatter = NumberFormatter()
        
        formatter.maximumFractionDigits = 4
        formatter.minimumFractionDigits = 4

        firstOperandValue.text = formatter.string(from: NSNumber(value: sender.value))
    }
    
    
    @IBAction func changeSecondOperand(_ sender: UISlider) {
        let formatter = NumberFormatter()
        
        formatter.maximumFractionDigits = 4
        formatter.minimumFractionDigits = 4

        secondOperandValue.text = formatter.string(from: NSNumber(value: sender.value))
    }
    
    
    @IBAction func calcResult(_ sender: UIButton) {
        let formatter = NumberFormatter()
        
        formatter.maximumFractionDigits = 4
        
        resultLabel.text = formatter.string(from: NSNumber(value: Double(slider.value) * stepper.value))
    }
}

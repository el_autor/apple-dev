//
//  CAKeyframeAnimationController.swift
//  Course2Week4Task2
//
//  Copyright © 2018 e-Legion. All rights reserved.
//

import UIKit

class CAKeyframeAnimationController: UIViewController {
    
    @IBOutlet weak var passcodeTextField: UITextField!
    @IBOutlet weak var orangeView: UIView!
    @IBOutlet weak var startShakeAnimationButton: UIButton!
    @IBOutlet weak var startSinAnimationButton: UIButton!
    
    @IBAction func shakeAnimationTapHandler(sender: UIButton) {
        let animation = CAKeyframeAnimation(keyPath: #keyPath(CALayer.position))
        let xPositions = [0, 10, -10, 10, -5, 5, -5, 0]
        animation.values = xPositions.reduce(into: [], {
            var point = self.passcodeTextField.center
            point.x += CGFloat($1)
            $0?.append(point)
        })
        animation.keyTimes = [0, 0.125, 0.25, 0.375, 0.5, 0.625, 0.8, 1]
        animation.duration = 0.4
        passcodeTextField.layer.add(animation, forKey: "shake")
    }

    @IBAction func sinAnimationTapHandler(sender: UIButton) {
        let animation = CAKeyframeAnimation(keyPath: #keyPath(CALayer.position))
        animation.path = sinPath()
        animation.duration = 6
        animation.repeatCount = .infinity
        animation.rotationMode = .rotateAuto
        
        orangeView.layer.add(animation, forKey: "position")
    }

    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        showSinPath()
    }
}

extension CAKeyframeAnimationController {
    private func showSinPath() {
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = sinPath()
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = UIColor.black.cgColor
        view.layer.addSublayer(shapeLayer)
    }
    
    private func sinPath() -> CGPath {
        let startPoint = orangeView.center
        let width = self.view.bounds.width - 2 * startPoint.x
        let height = self.view.bounds.height / 6.0
        return CGPath.sinPath(startPoint: startPoint, width: width, height: height, periods: 1.5)
    }
}

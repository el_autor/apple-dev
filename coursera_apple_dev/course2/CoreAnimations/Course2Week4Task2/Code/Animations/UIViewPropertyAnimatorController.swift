//
//  UIViewPropertyAnimatorController.swift
//  Course2Week4Task2
//
//  Copyright © 2018 e-Legion. All rights reserved.
//

import UIKit

class UIViewPropertyAnimatorController: UIViewController {
    
    @IBOutlet weak var animationView: UIView!
    private var animator: UIViewPropertyAnimator!
    
    @IBAction func panGestureHandler(recognizer: UIPanGestureRecognizer) {
        switch recognizer.state {
        case .began:
            let timing = UISpringTimingParameters(mass: 2.0, stiffness: 30.0, damping: 7.0, initialVelocity: .zero)
            animator = UIViewPropertyAnimator(duration: 1, timingParameters: timing)
            animator.addAnimations({
                self.animationView.center.x += 300
                self.animationView.transform = CGAffineTransform(rotationAngle: .pi)
                self.animationView.frame.size.width *= 1.5
                self.animationView.frame.size.height *= 1.5
            })
            animator.pauseAnimation()
        case .changed:
            animator.fractionComplete = recognizer.translation(in: view).x / view.bounds.maxX
        case .ended:
            if recognizer.translation(in: view).x < view.center.x {
                animator.isReversed = true
            }
            animator.startAnimation()
        default:
            ()
        }
    }
}

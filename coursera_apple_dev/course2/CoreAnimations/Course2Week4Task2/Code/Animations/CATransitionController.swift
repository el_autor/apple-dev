//
//  CATransitionController.swift
//  Course2Week4Task2
//
//  Copyright © 2018 e-Legion. All rights reserved.
//

import UIKit

class CATransitionController: UIViewController, CAAnimationDelegate {
    
    @IBOutlet weak var textLabel: UILabel!
    
    @IBAction func swipeHandler(_ sender: UISwipeGestureRecognizer) {
        let animation = CATransition()
        animation.duration = 1
        animation.type = .moveIn
        animation.timingFunction = CAMediaTimingFunction(name: .easeOut)
        animation.delegate = self
        
        textLabel.layer.add(animation, forKey: nil)
        textLabel.text = "Sliding!"
        textLabel.textColor = .green
    }
    
    
    func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        let animation = CATransition()
        animation.duration = 1
        animation.type = CATransitionType.fade
        animation.timingFunction = CAMediaTimingFunction(name: .easeOut)
        
        textLabel.layer.add(animation, forKey: nil)
        textLabel.text = "Initial text"
        textLabel.textColor = .orange
    }

}

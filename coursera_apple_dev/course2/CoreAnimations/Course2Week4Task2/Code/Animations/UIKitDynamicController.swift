//
//  UIKitDynamicController.swift
//  Course2Week4Task2
//
//  Copyright © 2018 e-Legion. All rights reserved.
//

import UIKit

class UIKitDynamicController: UIViewController {
    
    @IBOutlet weak var anchorView: UIView!
    @IBOutlet weak var animationView: UIView!
    
    var attachment: UIAttachmentBehavior!
    var animator: UIDynamicAnimator!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        anchorView.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(actionMoveAnchor(sender:))))
        
        attachment = UIAttachmentBehavior(item: animationView, attachedToAnchor: anchorView.center)
        attachment.length = 100
        attachment.frequency = 10
        attachment.damping = 5
        
        animator = UIDynamicAnimator(referenceView: view)
        animator.addBehavior(attachment)
        animator.addBehavior(UIGravityBehavior(items: [animationView]))
    }
    
    @objc private func actionMoveAnchor(sender: UIPanGestureRecognizer) {
        anchorView.center = sender.location(in: view)
        attachment.anchorPoint = sender.location(in: view)
    }
}

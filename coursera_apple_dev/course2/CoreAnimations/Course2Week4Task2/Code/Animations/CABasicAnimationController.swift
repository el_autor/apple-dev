//
//  ViewController.swift
//  Course2Week4Task2
//
//  Copyright © 2018 e-Legion. All rights reserved.
//

import UIKit

class CABasicAnimationController: UIViewController {
    
    @IBOutlet weak var orangeView: UIView!
    @IBOutlet weak var cyanView: UIView!
    @IBOutlet weak var blueView: UIView!
    @IBOutlet weak var greenView: UIView!
    
    
    override func viewDidLoad() {
        addTapGestureRecognizerOrange()
        addTapGestureRecognizerCyan()
        addTapGestureRecognizerBlue()
        addTapGestureRecognizerGreen()
    }
    
    private func addTapGestureRecognizerOrange() {
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(performCABasicAnimationOrange))
        recognizer.numberOfTouchesRequired = 1
        orangeView.addGestureRecognizer(recognizer)
    }

    private func addTapGestureRecognizerCyan() {
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(performCABasicAnimationCyan))
        recognizer.numberOfTouchesRequired = 1
        cyanView.addGestureRecognizer(recognizer)
    }

    private func addTapGestureRecognizerBlue() {
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(performCABasicAnimationBlue))
        recognizer.numberOfTouchesRequired = 1
        blueView.addGestureRecognizer(recognizer)
    }

    private func addTapGestureRecognizerGreen() {
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(performCABasicAnimationGreen))
        recognizer.numberOfTouchesRequired = 1
        greenView.addGestureRecognizer(recognizer)
    }

    
    @objc private func performCABasicAnimationOrange() {
        let targetValue = orangeView.bounds.height / 2
        let animation = CABasicAnimation(keyPath: #keyPath(CALayer.cornerRadius))
        animation.duration = 1
        animation.fromValue = orangeView.layer.cornerRadius
        animation.toValue = orangeView.bounds.height / 2
        animation.timingFunction = CAMediaTimingFunction(name: .easeIn)
        orangeView.layer.add(animation, forKey: "cornerRadius")
        orangeView.layer.cornerRadius = targetValue
    }

    @objc private func performCABasicAnimationCyan() {
        let targetValue = 0
        let animation = CABasicAnimation(keyPath: #keyPath(CALayer.opacity))
        animation.duration = 1
        animation.fromValue = cyanView.layer.opacity
        animation.toValue = targetValue
        animation.timingFunction = CAMediaTimingFunction(name: .easeIn)
        cyanView.layer.add(animation, forKey: "opacity")
        cyanView.layer.opacity = Float(targetValue)
    }
    
    @objc private func performCABasicAnimationBlue() {
        var newPosition = blueView.center
        newPosition.x = cyanView.center.x
        
        let positionAnimation = CABasicAnimation(keyPath: #keyPath(CALayer.position))
        positionAnimation.fromValue = blueView.center
        positionAnimation.toValue = newPosition
        
        let radians = CGFloat(315 * (Double.pi / 180))
        let finalTransformation = CATransform3DMakeRotation(radians, 0, 0, 1)
        let transformAnimation = CABasicAnimation(keyPath: "transform.rotation")
        transformAnimation.fromValue = 0
        transformAnimation.toValue = radians
        
        let group = CAAnimationGroup()
        group.animations = [positionAnimation, transformAnimation]
        group.duration = 2
        group.timingFunction = CAMediaTimingFunction(name: .easeInEaseOut)
        blueView.layer.add(group, forKey: "move&rotate")
        blueView.layer.transform = finalTransformation
        blueView.layer.position = newPosition
    }
    
    @objc private func performCABasicAnimationGreen() {
        let moveAnimation = CABasicAnimation(keyPath: #keyPath(CALayer.position))
        moveAnimation.fromValue = greenView.layer.position
        moveAnimation.toValue = view.center
        
        let scaleAnimation = CABasicAnimation(keyPath: "transform.scale")
        scaleAnimation.fromValue = greenView.layer.contentsScale
        scaleAnimation.toValue = 1.5
        
        let colorAnimation = CABasicAnimation(keyPath: #keyPath(CALayer.backgroundColor))
        colorAnimation.fromValue = greenView.layer.backgroundColor
        colorAnimation.toValue = UIColor.magenta.cgColor
        
        let group = CAAnimationGroup()
        group.animations = [moveAnimation, scaleAnimation, colorAnimation]
        group.timingFunction = CAMediaTimingFunction(name: .easeInEaseOut)
        group.duration = 1
        group.autoreverses = true
        group.repeatCount = 2
        greenView.layer.add(group, forKey: "move&scale&colorize")
    }
}

//
//  UIViewAnimationController.swift
//  Course2Week4Task2
//
//  Copyright © 2018 e-Legion. All rights reserved.
//

import UIKit

class UIViewAnimationController: UIViewController {
    
    @IBOutlet weak var animationView: UIView!
    @IBOutlet weak var flipButton: UIButton!
    
    @IBAction func animationViewTapHandler(sender: UITapGestureRecognizer) {
        UIView.animate(withDuration: 1, delay: 0.5, options: [.curveEaseOut], animations: {
            var newCenter = self.animationView.center
            newCenter.x = self.view.frame.maxX - (self.animationView.frame.width / 2)
            self.animationView.transform = self.animationView.transform.rotated(by: CGFloat(Double.pi))
            self.animationView.center = newCenter
        }, completion: nil)
    }
    
    @IBAction func flipButtonTapHandler(sender: UIButton) {
        UIView.animate(withDuration: 1, delay: 0, options: [.curveEaseInOut], animations: {
            self.view.transform = self.view.transform.rotated(by: CGFloat(Double.pi))
            self.flipButton.transform = self.flipButton.transform.rotated(by: CGFloat(Double.pi))
        }, completion: nil)
    }
}

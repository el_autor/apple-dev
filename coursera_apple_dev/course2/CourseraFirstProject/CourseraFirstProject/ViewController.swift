//
//  ViewController.swift
//  CourseraFirstProject
//
//  Created by Vlad on 26.07.2020.
//  Copyright © 2020 Vlad Krivozubov. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var secondLabel: UILabel!
    
    @IBAction func changeButtonTitlePressed(_ sender: Any) {
        titleLabel.text = "borrachinno"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = "Заголовок"
        titleLabel.sizeToFit()
        
        
        secondLabel.text = "Подвал"
        secondLabel.sizeToFit()
    }


}

//
//  ViewController.swift
//  debugApp
//
//  Created by Vlad on 29.07.2020.
//  Copyright © 2020 Vlad Krivozubov. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    let a = 5
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let b = 7
        myFunc()
        print(b)
    }

    func myFunc() {
        var c = 9
        c = 15
        print(c)
    }

}


//
//  RadioViewController.swift
//  Course2Week4Task1
//
//  Copyright © 2018 e-Legion. All rights reserved.
//

import UIKit

class RadioViewController: UIViewController {
    
    private var compactConstraints: [NSLayoutConstraint] = []
    private var regularConstraints: [NSLayoutConstraint] = []
    private var sharedConstraints: [NSLayoutConstraint] = []
    
    private lazy var songImage: UIImageView = {
        let image = UIImageView()
        
        image.translatesAutoresizingMaskIntoConstraints = false
        image.image = UIImage(named: "shake_wes")
        image.contentMode = .scaleAspectFill
        
        return image
    }()
    
    private lazy var progressBar: UIProgressView = {
        let progress = UIProgressView()
        
        progress.translatesAutoresizingMaskIntoConstraints = false
        progress.progress = 0.5
        
        return progress
    }()
    
    private lazy var songLabel: UILabel = {
        let label = UILabel()
        
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Aerosmith - Hole In My Soul"
        label.textAlignment = .center
        label.font = label.font.withSize(22)
        
        return label
    }()
    
    private lazy var songSlider: UISlider = {
        let slider = UISlider()
        
        slider.translatesAutoresizingMaskIntoConstraints = false
        slider.value = 0.5
        
        return slider
    }()
    
    private var labelLayoutContainer = UILayoutGuide()
    
    private func addSubviews() {
        self.view.addSubview(songImage)
        self.view.addSubview(progressBar)
        self.view.addLayoutGuide(labelLayoutContainer)
        self.view.addSubview(songLabel)
        self.view.addSubview(songSlider)
    }
    
    private func setupImageConstraints() {
        let topImageConstant: CGFloat = 8
        let leadingImageConstant: CGFloat = 16
        let trailingImageConstant: CGFloat = 16
        
        sharedConstraints.append(contentsOf: [
            songImage.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: leadingImageConstant)
        ])
        
        regularConstraints.append(contentsOf: [
            songImage.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: topImageConstant),
            songImage.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            songImage.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -trailingImageConstant),
            songImage.heightAnchor.constraint(equalToConstant: imageRegularSize()),
            songImage.bottomAnchor.constraint(equalTo: progressBar.topAnchor, constant: -CGFloat(30))
        ])
        
        compactConstraints.append(contentsOf: [
            songImage.topAnchor.constraint(equalTo: progressBar.bottomAnchor, constant: leadingImageConstant),
            songImage.bottomAnchor.constraint(equalTo: songSlider.topAnchor, constant: -leadingImageConstant),
            songImage.widthAnchor.constraint(equalToConstant: imageCompactSize())
        ])
    }
    
    private func imageRegularSize() -> CGFloat {
        let leadingImageConstant: CGFloat = 16
        let trailingImageConstant: CGFloat = 16
        
        if UIScreen.main.traitCollection.verticalSizeClass == .regular {
            return CGFloat(view.bounds.width - (leadingImageConstant + trailingImageConstant))
        }
        else {
            return CGFloat(view.bounds.height - (leadingImageConstant + trailingImageConstant))
        }
    }
    
    private func imageCompactSize() -> CGFloat {
        let bounds: CGFloat = 16
        let sliderHeight = songSlider.bounds.height
        let progressBarHeight = progressBar.bounds.height
        let delta: CGFloat = 24
        
        if UIScreen.main.traitCollection.verticalSizeClass == .compact {
            return view.bounds.height - (3 * bounds + delta + sliderHeight + progressBarHeight)
        }
        else {
            return view.bounds.width - (3 * bounds + delta + sliderHeight + progressBarHeight)
        }
        
    }
    
    
    private func setupProgressBarConstraints() {
        let leadingProgressBarConstant: CGFloat = 16
        let trailingProgressBarConstant: CGFloat = 16
        
        sharedConstraints.append(contentsOf: [
            progressBar.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: leadingProgressBarConstant),
            progressBar.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -trailingProgressBarConstant)
        ])
        
        compactConstraints.append(contentsOf: [
            progressBar.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: leadingProgressBarConstant)
        ])
    }
    
    private func setupLabelConstraints() {
        let leadingContainerConstant: CGFloat = 16
        let trailingContainerConstant: CGFloat = 16
        
        sharedConstraints.append(contentsOf: [
            songLabel.centerYAnchor.constraint(equalTo: labelLayoutContainer.centerYAnchor),
            songLabel.centerXAnchor.constraint(equalTo: labelLayoutContainer.centerXAnchor),
            labelLayoutContainer.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -trailingContainerConstant),
            labelLayoutContainer.topAnchor.constraint(equalTo: progressBar.bottomAnchor),
            labelLayoutContainer.bottomAnchor.constraint(equalTo: songSlider.topAnchor)
        ])
        
        regularConstraints.append(contentsOf: [
            labelLayoutContainer.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: leadingContainerConstant)
        ])
        
        compactConstraints.append(contentsOf: [
            labelLayoutContainer.leadingAnchor.constraint(equalTo: songImage.trailingAnchor, constant: -leadingContainerConstant)
        ])
    }
    
    private func setupSliderConstraints() {
        let leadingSliderConstant: CGFloat = 16
        let trailingSliderConstant: CGFloat = 16
        let sliderHeight: CGFloat = 31
        let bottomSliderConstant: CGFloat = 24
        
        sharedConstraints.append(contentsOf: [
            songSlider.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: leadingSliderConstant),
            songSlider.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -trailingSliderConstant),
            songSlider.heightAnchor.constraint(equalToConstant: sliderHeight),
            songSlider.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -bottomSliderConstant)
        ])
    }
    
    private func setupConstraints() {
        setupProgressBarConstraints()
        setupLabelConstraints()
        setupSliderConstraints()
        setupImageConstraints()
    }
    
    private func updateConstraints(traitCollection: UITraitCollection) {
        if !sharedConstraints[0].isActive {
            NSLayoutConstraint.activate(sharedConstraints)
        }
        
        if traitCollection.verticalSizeClass == .regular {
            if compactConstraints[0].isActive {
                NSLayoutConstraint.deactivate(compactConstraints)
            }
            NSLayoutConstraint.activate(regularConstraints)
        }
        else if traitCollection.verticalSizeClass == .compact {
            if regularConstraints[0].isActive {
                NSLayoutConstraint.deactivate(regularConstraints)
            }
            NSLayoutConstraint.activate(compactConstraints)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = .white
        addSubviews()
        setupConstraints()
        
        NSLayoutConstraint.activate(sharedConstraints)
        updateConstraints(traitCollection: UIScreen.main.traitCollection)
    }
    
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        
        updateConstraints(traitCollection: UIScreen.main.traitCollection)
    }
}

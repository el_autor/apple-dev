//
//  PhotoCell.swift
//  Course2FinalTask
//
//  Created by Vlad on 26.09.2020.
//  Copyright © 2020 e-Legion. All rights reserved.
//

import UIKit
import DataProvider

class PhotoCell: UICollectionViewCell {
    private lazy var photoView: UIImageView = {
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.frame = self.bounds
        return imageView
    }()
    
    // MARK: - init
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.addSubview(photoView)
        
        photoView.widthAnchor.constraint(equalToConstant: self.frame.size.width).isActive = true
        photoView.heightAnchor.constraint(equalToConstant: self.frame.size.height).isActive = true
        
        /*
        let selectRecognizer = UITapGestureRecognizer(target: self, action: #selector(resolveSelection))
        selectRecognizer.numberOfTapsRequired = 1
        addGestureRecognizer(selectRecognizer)
         */
    }

    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func configure(_ post: Post) {
        self.photoView.image = post.image
    }
    
    /*
    @objc private func resolveSelection() {
        if isSelected {
            isSelected = false
            photoView.layer.opacity = 1
        }
        else {
            isSelected = true
            photoView.layer.opacity = 0.5
        }
    }
    */
}

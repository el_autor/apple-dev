//
//  FeedViewController.swift
//  Course2FinalTask
//
//  Created by Vlad on 02.09.2020.
//  Copyright © 2020 e-Legion. All rights reserved.
//

import UIKit
import DataProvider

class FeedViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    private var data: [Post]!
    
    private let labelTag = 1
    private let tableViewTag = 2
    
    private var regularConstraints: [NSLayoutConstraint] = []
    
    private lazy var posts: UITableView = {
        let tableView = self.view.viewWithTag(tableViewTag) as! UITableView
        
        tableView.translatesAutoresizingMaskIntoConstraints = false
        
        return tableView
    }()
    
    private func setupTableViewConstraints() {
        let leadingConstant: CGFloat = 0
        let trailingConstant: CGFloat = 0
        let topConstant: CGFloat = 0
        let bottomConstant: CGFloat = 0
        
        regularConstraints.append(contentsOf: [
            posts.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: leadingConstant),
            posts.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: trailingConstant),
            posts.topAnchor.constraint(equalTo: self.view.topAnchor, constant: topConstant),
            posts.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: bottomConstant)
        ])
    }
    
    private func setupConstraints() {
        setupTableViewConstraints()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Feed"
        data = DataProviders.shared.postsDataProvider.feed()
        posts.dataSource = self
        posts.delegate = self
        posts.separatorStyle = .none
        //posts.allowsMultipleSelection = true
        
        let xib = UINib(nibName: "PostsCell", bundle: nil)
        posts.register(xib, forCellReuseIdentifier: "CustomCell")
        
        setupConstraints()
        
        NSLayoutConstraint.activate(regularConstraints)
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomCell", for: indexPath) as! PostsCell
        
        cell.configure(post: data[indexPath.row])
        cell.rootTableView = self
        
        return cell
    }
}

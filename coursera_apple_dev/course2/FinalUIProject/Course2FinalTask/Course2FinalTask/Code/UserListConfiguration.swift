enum UserListConfiguration: String {
    case likes = "Likes"
    case followers = "Followers"
    case following = "Following"
}

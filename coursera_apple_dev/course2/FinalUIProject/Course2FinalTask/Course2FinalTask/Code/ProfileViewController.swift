//
//  ProfileViewController.swift
//  Course2FinalTask
//
//  Created by Vlad on 22.09.2020.
//  Copyright © 2020 e-Legion. All rights reserved.
//

import UIKit
import DataProvider

class ProfileViewController: UIViewController {

    var user: User!
    var posts: [Post]!
    private var contentHeight: CGFloat!
    private var photosHeight: CGFloat!
    
    private var regularConstraints: [NSLayoutConstraint] = []
    
    private let cellID = "photoCell"
    
    private lazy var scrollView: UIScrollView = {
        let scrollable = UIScrollView()
        
        scrollable.translatesAutoresizingMaskIntoConstraints = false
        scrollable.backgroundColor = .white
        scrollable.contentSize = CGSize(width: UIScreen.main.bounds.size.width, height: self.contentHeight)
        
        return scrollable
    }()
    
    private lazy var contentView: UIView = {
        let view = UIView()
        
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        
        return view
    }()
    
    private lazy var userImage: UIImageView = {
        let image = UIImageView()
        let imageCornerRadius: CGFloat = 35
         
        image.contentMode = .scaleAspectFill
        image.translatesAutoresizingMaskIntoConstraints = false
        image.layer.cornerRadius = imageCornerRadius
        image.clipsToBounds = true
        
        return image
    }()
    
    private lazy var nameLabel: UILabel = {
        let label = UILabel()
        let fontSize: CGFloat = 14
        
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .left
        label.font = UIFont.systemFont(ofSize: fontSize)
        
        return label
    }()
    
    private lazy var followersLabel: UILabel = {
        let label = UILabel()
        let fontSize: CGFloat = 14
        
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .left
        label.font = UIFont.systemFont(ofSize: fontSize, weight: .semibold)
        
        return label
    }()
    
    private lazy var followingLabel: UILabel = {
        let label = UILabel()
        let fontSize: CGFloat = 14
        
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .right
        label.font = UIFont.systemFont(ofSize: fontSize, weight: .semibold)
        
        return label
    }()
    
    private lazy var photos: UICollectionView = {
        var layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        let screenRect = UIScreen.main.bounds
        let screenWidth = screenRect.size.width
        let cellWidth = screenWidth / 3
        let cellSize = CGSize(width: cellWidth, height: cellWidth)
        
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.itemSize = cellSize
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        
        var collection = UICollectionView(frame: self.view.frame, collectionViewLayout: layout)
        
        collection.register(PhotoCell.self, forCellWithReuseIdentifier: cellID)
        
        collection.translatesAutoresizingMaskIntoConstraints = false
        
        collection.dataSource = self
        collection.delegate = self
        collection.backgroundColor = .white
        collection.allowsSelection = true
        
        return collection
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        posts = DataProviders.shared.postsDataProvider.findPosts(by: user.id)
        
        let fullSections: CGFloat = CGFloat(posts.count / 3)
        
        if posts.count % 3 == 0 {
            self.photosHeight = fullSections * (UIScreen.main.bounds.size.width / 3)
        }
        else {
            self.photosHeight = (fullSections + 1) * (UIScreen.main.bounds.size.width / 3)
        }
        
        self.contentHeight = 86 + self.photosHeight
        
        view.backgroundColor = .white
        self.navigationItem.title = user.username
        
        addSubviews()
        setupConstraints()
        
        setupRecognizers()
        
        userImage.image = user.avatar
        nameLabel.text = user.fullName
        followersLabel.text = "Followers: \(user.followedByCount)"
        followingLabel.text = "Following: \(user.followsCount)"
        
        NSLayoutConstraint.activate(regularConstraints)
    }
    
    private func setupRecognizers(){
        let followersTapRecognizer = UITapGestureRecognizer(target: self, action: #selector(navigateToFollowersList))
        followersTapRecognizer.numberOfTapsRequired = 1
        followersLabel.addGestureRecognizer(followersTapRecognizer)
        followersLabel.isUserInteractionEnabled = true
        
        let followingTapRecognizer = UITapGestureRecognizer(target: self, action: #selector(navigateToFollowingList))
        followingTapRecognizer.numberOfTapsRequired = 1
        followingLabel.addGestureRecognizer(followingTapRecognizer)
        followingLabel.isUserInteractionEnabled = true
    }
    
    @objc private func navigateToFollowersList() {
        let followersController = UserListViewController()
        let followers = DataProviders.shared.usersDataProvider.usersFollowingUser(with: user.id)!.map{$0.id}
        
        followersController.users = followers
        followersController.listTitle = .followers
        
        self.navigationController?.pushViewController(followersController, animated: true)
    }
 
    @objc private func navigateToFollowingList() {
        let followingController = UserListViewController()
        let following = DataProviders.shared.usersDataProvider.usersFollowedByUser(with: user.id)!.map{$0.id}
        
        followingController.users = following
        followingController.listTitle = .following
        
        self.navigationController?.pushViewController(followingController, animated: true)
    }
    
    private func addSubviews() {
        view.addSubview(scrollView)
        scrollView.addSubview(contentView)
        contentView.addSubview(userImage)
        contentView.addSubview(nameLabel)
        contentView.addSubview(followersLabel)
        contentView.addSubview(followingLabel)
        contentView.addSubview(photos)
    }
    
    private func setupConstraints() {
        setupScrollViewConstraints()
        setupContentViewConstarints()
        setupUserImageConstraints()
        setupNameLabelConstraints()
        setupFollowersLabelConstraints()
        setupFollowingLabelConstraints()
        setupCollectionViewConstraints()
    }
    
    private func setupContentViewConstarints() {
        let topConstant: CGFloat = 0
        let leadingConstant: CGFloat = 0
        let trailingConstant: CGFloat = 0
        
        regularConstraints.append(contentsOf: [
            contentView.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: topConstant),
            contentView.leadingAnchor.constraint(equalTo: scrollView.safeAreaLayoutGuide.leadingAnchor, constant: leadingConstant),
            contentView.trailingAnchor.constraint(equalTo: scrollView.safeAreaLayoutGuide.trailingAnchor, constant: -trailingConstant)
        ])
    }
    
    private func setupScrollViewConstraints() {
        let topConstant: CGFloat = 0
        let leadingConstant: CGFloat = 0
        let trailingConstant: CGFloat = 0
        let bottomConstant: CGFloat = 0
        
        regularConstraints.append(contentsOf: [
            scrollView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: topConstant),
            scrollView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: leadingConstant),
            scrollView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -trailingConstant),
            scrollView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -bottomConstant)
        ])
    }
    
    private func setupCollectionViewConstraints() {
        let topConstant: CGFloat = 8
        let leadingConstant: CGFloat = 0
        let trailingConstant: CGFloat = 0
        let bottomConstant: CGFloat = 0
        
        regularConstraints.append(contentsOf: [
            photos.topAnchor.constraint(equalTo: followingLabel.bottomAnchor, constant: topConstant),
            photos.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: leadingConstant),
            photos.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -trailingConstant),
            photos.heightAnchor.constraint(equalToConstant: self.photosHeight),
            photos.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -bottomConstant)
        ])
    }
    
    private func setupUserImageConstraints() {
        let topConstant: CGFloat = 8
        let leadingConstant: CGFloat = 8
        let width: CGFloat = 70
        let height: CGFloat = 70
        
        regularConstraints.append(contentsOf: [
            userImage.topAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.topAnchor, constant: topConstant),
            userImage.leadingAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.leadingAnchor, constant: leadingConstant),
            userImage.widthAnchor.constraint(equalToConstant: width),
            userImage.heightAnchor.constraint(equalToConstant: height)
        ])
    }
    
    private func setupNameLabelConstraints() {
        let topConstant: CGFloat = 8
        let leadingConstant: CGFloat = 8
        let widthConstant: CGFloat = 100
        
        regularConstraints.append(contentsOf: [
            nameLabel.topAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.topAnchor, constant: topConstant),
            nameLabel.leadingAnchor.constraint(equalTo: userImage.trailingAnchor, constant: leadingConstant),
            nameLabel.widthAnchor.constraint(equalToConstant: widthConstant)
        ])
    }
    
    private func setupFollowersLabelConstraints() {
        let leadingConstant: CGFloat = 8
        let bottomConstant: CGFloat = 0
        
        regularConstraints.append(contentsOf: [
            followersLabel.leadingAnchor.constraint(equalTo: userImage.trailingAnchor, constant: leadingConstant),
            followersLabel.bottomAnchor.constraint(equalTo: userImage.bottomAnchor, constant: -bottomConstant)
        ])
    }
    
    private func setupFollowingLabelConstraints() {
        let trailingConstant: CGFloat = 16
        let bottomConstant: CGFloat = 0
        
        regularConstraints.append(contentsOf: [
            followingLabel.trailingAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.trailingAnchor, constant: -trailingConstant),
            followingLabel.bottomAnchor.constraint(equalTo: userImage.bottomAnchor, constant: -bottomConstant)
        ])
    }
}

extension ProfileViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let posts = self.posts else { return 0 }
        return posts.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellID, for: indexPath) as! PhotoCell
        
        cell.configure(posts[indexPath.row])
        
        return cell
    }
}

extension ProfileViewController: UICollectionViewDelegate {
    
}

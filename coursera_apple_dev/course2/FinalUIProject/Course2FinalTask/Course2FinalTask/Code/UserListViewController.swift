//
//  UserListViewController.swift
//  Course2FinalTask
//
//  Created by Vlad on 22.09.2020.
//  Copyright © 2020 e-Legion. All rights reserved.
//

import UIKit
import DataProvider

class UserListViewController: UIViewController, UITableViewDataSource {

    private let cellIdentifier = "defaultCell"
    
    private var regularConstraints: [NSLayoutConstraint] = []
    
    var configuration: UserListConfiguration!
    var users: [User.Identifier]!
    var listTitle: UserListConfiguration!
    
    lazy var tableView: UITableView = {
        var view = UITableView()
        
        view.translatesAutoresizingMaskIntoConstraints = false
        view.dataSource = self
        
        view.register(UserListCell.self, forCellReuseIdentifier: self.cellIdentifier)
        
        return view
    } ()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = listTitle.rawValue
        
        addSubviews()
        setupConstraints()
        
        NSLayoutConstraint.activate(regularConstraints)
    }
    
    private func addSubviews() {
        view.addSubview(tableView)
    }
    
    private func setupConstraints() {
        setupTableViewConstraints()
    }
    
    private func setupTableViewConstraints() {
        let upConstant: CGFloat = 0
        let bottomConstant: CGFloat = 0
        let leadingConstant: CGFloat = 0
        let trailingConstant: CGFloat = 0
        
        regularConstraints.append(contentsOf: [
            tableView.topAnchor.constraint(equalTo: view.topAnchor, constant: upConstant),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -bottomConstant),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: leadingConstant),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -trailingConstant)
        ])
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! UserListCell
        
        cell.rootTableView = self
        cell.configure(user: DataProviders.shared.usersDataProvider.user(with: users[indexPath.row])!)
        
        return cell
    }
}

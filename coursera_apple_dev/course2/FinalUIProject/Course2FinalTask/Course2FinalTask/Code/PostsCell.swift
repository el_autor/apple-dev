//
//  PostsCell.swift
//  Course2FinalTask
//
//  Created by Vlad on 03.09.2020.
//  Copyright © 2020 e-Legion. All rights reserved.
//

import UIKit
import DataProvider

class PostsCell: UITableViewCell {

    private var postData: [Post?] = []
    var rootTableView: FeedViewController!
    
    private let postAuthorTag = 1
    private let postAuthorNameTag = 2
    private let postTimingLabelTag = 3
    private let imageTag = 4
    private let likesLabelTag = 5
    private let likeImageTag = 6
    private let postTextTag = 7
    
    private var regularConstraints: [NSLayoutConstraint] = []
    
    private var heightConstraint: NSLayoutConstraint?
    
    lazy var userImage: UIImageView = {
        let image = self.viewWithTag(postAuthorTag) as! UIImageView
        
        image.contentMode = .scaleAspectFill
        image.translatesAutoresizingMaskIntoConstraints = false
        
        return image
    }()
    
    lazy var authorLabel: UILabel = {
        let fontSize: CGFloat = 14
        let label = self.viewWithTag(postAuthorNameTag) as! UILabel
    
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: fontSize, weight: .semibold)
        label.textColor = .black
        
        return label
    }()
    
    lazy var postTimeLabel: UILabel = {
        let fontSize: CGFloat = 14
        let label = self.viewWithTag(postTimingLabelTag) as! UILabel
        
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: fontSize)
        label.textColor = .black
            
        return label
    }()
    
    lazy var postImage: UIImageView = {
        let image = self.viewWithTag(imageTag) as! UIImageView
        
        image.contentMode = .scaleAspectFill
        image.translatesAutoresizingMaskIntoConstraints = false
        
        return image
    }()
    
    lazy var likesLabel: UILabel = {
        let fontSize: CGFloat = 14
        let label = self.viewWithTag(likesLabelTag) as! UILabel
        
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: fontSize, weight: .semibold)
        label.textColor = .black
        
        return label
    } ()
    
    lazy var likeImage: UIImageView = {
        let image = self.viewWithTag(likeImageTag) as! UIImageView
        
        image.contentMode = .center
        image.translatesAutoresizingMaskIntoConstraints = false
        
        return image
    } ()
    
    lazy var postText: UILabel = {
        let text = UILabel()
        
        text.translatesAutoresizingMaskIntoConstraints = false
        text.numberOfLines = 0
        text.lineBreakMode = .byWordWrapping
        text.font = UIFont.systemFont(ofSize: 14)
        
        return text
    } ()
    
    lazy var bigLikeView: UIImageView = {
        let image = UIImageView()
        
        image.translatesAutoresizingMaskIntoConstraints = false
        image.image = UIImage(named: "bigLike")
        image.tintColor = .white
        image.layer.opacity = 0
        
        return image
    } ()
    
    override func awakeFromNib() {
        super.awakeFromNib()
    
        selectionStyle = .none
        addSubviews()
        setupConstraints()
        setupRecognizers()
        
        NSLayoutConstraint.activate(regularConstraints)
    }
    
    private func setupRecognizers() {
        let likeTapRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(setLike))
        
        likeTapRecognizer.numberOfTapsRequired = 1
        likeImage.addGestureRecognizer(likeTapRecognizer)
        likeImage.isUserInteractionEnabled = true
        
        let imageLikeRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(setBigLike))
        
        imageLikeRecognizer.numberOfTapsRequired = 2
        postImage.addGestureRecognizer(imageLikeRecognizer)
        postImage.isUserInteractionEnabled = true
        
        let likesTapRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(setupLikesController))
        
        likesTapRecognizer.numberOfTapsRequired = 1
        likesLabel.addGestureRecognizer(likesTapRecognizer)
        likesLabel.isUserInteractionEnabled = true
        
        let userImageTapRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(navigateToUserProfile)
        )
        
        userImageTapRecognizer.numberOfTapsRequired = 1
        userImage.addGestureRecognizer(userImageTapRecognizer)
        userImage.isUserInteractionEnabled = true
        
        let usernameTapRecognizer = UITapGestureRecognizer(
            target: self, action: #selector(navigateToUserProfile)
        )
        
        usernameTapRecognizer.numberOfTapsRequired = 1
        authorLabel.addGestureRecognizer(usernameTapRecognizer)
        authorLabel.isUserInteractionEnabled = true

    }
    
    @objc func navigateToUserProfile(_ sender: UITapGestureRecognizer) {
        let profile = ProfileViewController()
        profile.user = DataProviders.shared.usersDataProvider.user(with: postData[0]!.author)
        
        self.rootTableView.navigationController?.pushViewController(profile, animated: true)
    }
    
    @objc func setupLikesController(_ sender: UITapGestureRecognizer) {
        let likesController = UserListViewController()
        let likedUsers = DataProviders.shared.postsDataProvider.usersLikedPost(with: postData[0]!.id)
        
        guard let users = likedUsers else { return }
            
        likesController.users = users
        likesController.listTitle = .likes
        
        self.rootTableView.navigationController?.pushViewController(likesController, animated: true)
    }
    
    @objc func setLike(_ sender: UITapGestureRecognizer) {
        print("Like image tap")
        
        guard let currentPost = postData[0] else { return }
        
        if currentPost.currentUserLikesThisPost {
            _ = DataProviders.shared.postsDataProvider.unlikePost(with: currentPost.id)
        }
        else {
            _ = DataProviders.shared.postsDataProvider.likePost(with: currentPost.id)
        }
        
        guard let newPost = DataProviders.shared.postsDataProvider.post(with: currentPost.id) else { return }
        
        self.configure(post: newPost)
    }
    
    @objc func setBigLike(_ sender: UITapGestureRecognizer) {
        print("Big like!")
        
        guard let currentPost = postData[0] else { return }
        
        if !currentPost.currentUserLikesThisPost {
            _ = DataProviders.shared.postsDataProvider.likePost(with: currentPost.id)
            
            let bigLikeAnimation = CAKeyframeAnimation(keyPath: "opacity")
            bigLikeAnimation.values = [0, 1, 1, 1, 0, 0, 0]
            bigLikeAnimation.keyTimes = [0, 0.17, 0.33, 0.5, 0.66, 0.83, 1]
            bigLikeAnimation.duration = 0.6
            bigLikeAnimation.timingFunction = CAMediaTimingFunction(name: .easeIn)
    
            bigLikeView.layer.add(bigLikeAnimation, forKey: "bigLike")
            
            guard let newPost = DataProviders.shared.postsDataProvider.post(with: currentPost.id) else { return }
            
            self.configure(post: newPost)
        }
    }
    
    private func addSubviews() {
        addSubview(postText)
        addSubview(bigLikeView)
    }
    
    private func setupConstraints() {
        setupPostAuthorImageConstraints()
        setupAuthorLabelConstraints()
        setupPostDataConstraints()
        setupPostImageConstraints()
        setupLikesLabelConstraints()
        setupLikeImageConstraint()
        setupPostTextConstraints()
        setupBigLikeViewConstraint()
    }
    
    private func setupBigLikeViewConstraint() {
        regularConstraints.append(contentsOf: [
            bigLikeView.centerXAnchor.constraint(equalTo: postImage.centerXAnchor),
            bigLikeView.centerYAnchor.constraint(equalTo: postImage.centerYAnchor)
        ])
    }
    
    private func setupPostAuthorImageConstraints() {
        let leftConstant: CGFloat = 15
        let topConstant: CGFloat = 8
        let widthConstant: CGFloat = 35
        let heightConstant: CGFloat = 35
        
        regularConstraints.append(contentsOf: [
            userImage.leftAnchor.constraint(equalTo: self.leftAnchor, constant: leftConstant),
            userImage.topAnchor.constraint(equalTo: self.topAnchor, constant: topConstant),
            userImage.widthAnchor.constraint(equalToConstant: widthConstant),
            userImage.heightAnchor.constraint(equalToConstant: heightConstant)
        ])
    }
    
    private func setupAuthorLabelConstraints() {
        let leftConstant: CGFloat = 8
        let topConstant: CGFloat = 8
        let rightConstant: CGFloat = 0
        
        regularConstraints.append(contentsOf: [
            authorLabel.leftAnchor.constraint(equalTo: userImage.rightAnchor, constant: leftConstant),
            authorLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: topConstant),
            authorLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: rightConstant)
        ])
    }
    
    private func setupPostDataConstraints() {
        let leftConstant: CGFloat = 8
        let downConstant: CGFloat = 8
        let rightConstant: CGFloat = 0
        let topConstant: CGFloat = 1
        
        regularConstraints.append(contentsOf: [
            postTimeLabel.leftAnchor.constraint(equalTo: userImage.rightAnchor, constant: leftConstant),
            postTimeLabel.bottomAnchor.constraint(equalTo: postImage.topAnchor, constant: -downConstant),
            postTimeLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: rightConstant),
            postTimeLabel.topAnchor.constraint(equalTo: authorLabel.bottomAnchor, constant: topConstant)
        ])
    }
    
    private func setupPostImageConstraints() {
        let leftConstant: CGFloat = 0
        let rightConstant: CGFloat = 0
        
        regularConstraints.append(contentsOf: [
            postImage.leftAnchor.constraint(equalTo: self.leftAnchor, constant: leftConstant),
            postImage.rightAnchor.constraint(equalTo: self.rightAnchor, constant: rightConstant),
            postImage.heightAnchor.constraint(equalToConstant: UIScreen.main.bounds.width)
        ])
    }
    
    private func setupLikesLabelConstraints() {
        let leftConstant: CGFloat = 15
        
        regularConstraints.append(contentsOf: [
            likesLabel.leftAnchor.constraint(equalTo: self.leftAnchor, constant: leftConstant),
            likesLabel.centerYAnchor.constraint(equalTo: likeImage.centerYAnchor, constant: 0)
        ])
    }
    
    private func setupLikeImageConstraint() {
        let height: CGFloat = 44
        let width: CGFloat = height
        let trailingConstant: CGFloat = 15
        let topConstant: CGFloat = 0
        
        regularConstraints.append(contentsOf: [
            likeImage.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -trailingConstant),
            likeImage.topAnchor.constraint(equalTo: postImage.bottomAnchor, constant: topConstant),
            likeImage.heightAnchor.constraint(equalToConstant: height),
            likeImage.widthAnchor.constraint(equalToConstant: width)
        ])
    }
    
    private func setupPostTextConstraints() {
        let leftConstant: CGFloat = 15
        let rightConstant: CGFloat = 15
        let topConstant: CGFloat = 0
        
        regularConstraints.append(contentsOf: [
            postText.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: leftConstant),
            postText.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -rightConstant),
            postText.topAnchor.constraint(equalTo: likeImage.bottomAnchor, constant: topConstant)
        ])
    }
    
    func setupDynamicHeight() {
        let height: CGFloat = 51 + UIScreen.main.bounds.width + 44 + postText.bounds.height
        heightConstraint = contentView.heightAnchor.constraint(equalToConstant: height)
        heightConstraint?.isActive = true
    }
    
    func configure(post: Post) {
        self.postData = []
        self.postData.append(post)
        
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        formatter.timeStyle = .medium
        formatter.locale = Locale(identifier: "en_US")
        
        userImage.image = post.authorAvatar
        authorLabel.text = post.authorUsername
        postTimeLabel.text = formatter.string(from: post.createdTime)
        postImage.image = post.image
        
        likeImage.image = UIImage(named: "like")
        if post.currentUserLikesThisPost {
            likeImage.tintColor = .systemBlue
        }
        else {
            likeImage.tintColor = .lightGray
        }
        
        likesLabel.text = "Likes: \(post.likedByCount)"
        postText.text = post.description
        postText.sizeToFit()
        postText.layoutIfNeeded()
        
        if let heightConstraint = heightConstraint {
            contentView.removeConstraint(heightConstraint)
            //print("removing")
        }
        //print("setupping")
        setupDynamicHeight()
    }
}

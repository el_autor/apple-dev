//
//  AppDelegate.swift
//  Course2FinalTask
//
//  Copyright © 2018 e-Legion. All rights reserved.
//

import UIKit
import DataProvider

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    
        window = UIWindow(frame: UIScreen.main.bounds)
        
        let tabViewController = UITabBarController()
        let feedVC = FeedViewController()
        let profileVC = ProfileViewController()
        profileVC.user = DataProviders.shared.usersDataProvider.currentUser()
        initFeedTabBar(viewController: feedVC)
        initProfileTabBar(viewController: profileVC)
        
        let feedNavigationVC = UINavigationController(rootViewController: feedVC)
        let profileNavigationVC = UINavigationController(rootViewController: profileVC)
        
        tabViewController.viewControllers = [feedNavigationVC, profileNavigationVC]
        tabViewController.selectedViewController = tabViewController.viewControllers?.first
        
        window?.rootViewController = tabViewController
        window?.makeKeyAndVisible()
         
        return true
    }
    
    private func initFeedTabBar(viewController: UIViewController) {
        let item = UITabBarItem()
        item.title = "Feed"
        item.image = UIImage(named: "feed")
        viewController.tabBarItem = item
        viewController.tabBarItem.isEnabled = true
    }
    
    private func initProfileTabBar(viewController: UIViewController) {
        let item = UITabBarItem()
        item.title = "Profile"
        item.image = UIImage(named: "profile")
        viewController.tabBarItem = item
        viewController.tabBarItem.isEnabled = true
    }

}

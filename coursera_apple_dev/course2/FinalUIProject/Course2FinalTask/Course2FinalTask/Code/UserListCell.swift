//
//  UserListCell.swift
//  Course2FinalTask
//
//  Created by Vlad on 22.09.2020.
//  Copyright © 2020 e-Legion. All rights reserved.
//

import UIKit
import DataProvider

class UserListCell: UITableViewCell {
    
    private var user: User!
    
    var rootTableView: UserListViewController!
    
    private var regularConstraints: [NSLayoutConstraint] = []
    
    lazy var userPhoto: UIImageView = {
        let image = UIImageView()
        
        image.contentMode = .scaleAspectFill
        image.translatesAutoresizingMaskIntoConstraints = false
        
        return image
    }()
    
    lazy var userName: UILabel = {
        let label = UILabel()
        
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .left
        
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        addSubviews()
        setupConstraints()
        setupGestureRecognizers()
        
        NSLayoutConstraint.activate(regularConstraints)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    private func addSubviews() {
        addSubview(userPhoto)
        addSubview(userName)
    }
    
    private func setupConstraints() {
        setupUserPhotoConstraints()
        setupUserNameConstraints()
    }
    
    private func setupUserPhotoConstraints() {
        let topConstant: CGFloat = 0
        let leadingConstant: CGFloat = 15
        let widthConstraint: CGFloat = 45
        let heightConstraint: CGFloat = 45
        
        regularConstraints.append(contentsOf: [
            userPhoto.topAnchor.constraint(equalTo: topAnchor, constant: topConstant),
            userPhoto.leadingAnchor.constraint(equalTo: leadingAnchor, constant: leadingConstant),
            userPhoto.widthAnchor.constraint(equalToConstant: widthConstraint),
            userPhoto.heightAnchor.constraint(equalToConstant: heightConstraint)
        ])
    }
    
    private func setupUserNameConstraints() {
        let leadingConstant: CGFloat = 16
        let width: CGFloat = 100
        
        regularConstraints.append(contentsOf: [
            userName.leadingAnchor.constraint(equalTo: userPhoto.trailingAnchor, constant: leadingConstant),
            userName.widthAnchor.constraint(equalToConstant: width),
            userName.centerYAnchor.constraint(equalTo: userPhoto.centerYAnchor)
        ])
    }

    func configure(user: User) {
        self.user = user
        
        userPhoto.image = user.avatar
        userName.text = user.fullName
    }
    
    private func setupGestureRecognizers() {
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(navigateToUserView))
        tapRecognizer.numberOfTapsRequired = 1
        contentView.addGestureRecognizer(tapRecognizer)
        contentView.isUserInteractionEnabled = true
    }
    
    @objc private func navigateToUserView(_ sender: UITapGestureRecognizer) {
        isSelected = true
        let userViewController = ProfileViewController()
        userViewController.user = DataProviders.shared.usersDataProvider.user(with: self.user.id)
        
        self.rootTableView.navigationController?.pushViewController(userViewController, animated: true)
        isSelected = false
    }
}

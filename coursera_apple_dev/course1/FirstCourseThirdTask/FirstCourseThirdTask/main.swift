// Этот файл пуст не по ошибке. В этот раз вам необходимо самостоятельно импортировать необходимые модули и запустить проверку

import Foundation
import FirstCourseThirdTaskChecker

var checker = Checker()

class Stack : FirstCourseThirdTaskChecker.ArrayInitializableStorage{
    private var storage: [Int]
    override var count: Int {
        get{
            return storage.count
        }
    }
    
    required init() {
        self.storage = []
        super.init()
    }
    required init(array: [Int]) {
        self.storage = array
        super.init()
    }
        
    override func push(_ element: Int) {
        storage.append(element)
    }
    
    override func pop() -> Int {
        let element = self.storage[count - 1]
        self.storage = [Int](self.storage[0..<count - 1])
        return element
    }
}

class Queue : FirstCourseThirdTaskChecker.ArrayInitializableStorage{
    private var storage: [Int]
    override var count: Int {
        get{
            return storage.count
        }
    }
    
    required init() {
        self.storage = []
        super.init()
    }
    
    required init(array: [Int]) {
        self.storage = array
        super.init()
    }
    
    override func push(_ element: Int) {
        storage.append(element)
    }
    
    override func pop() -> Int {
        let element = self.storage[0]
        self.storage = [Int](self.storage[1..<count])
        return element
    }
}

checker.checkInheritance(stack: Stack(), queue: Queue())

struct ProtocolStack : FirstCourseThirdTaskChecker.ArrayInitializable,                        FirstCourseThirdTaskChecker.StorageProtocol {
    private var storage: [Int]
    var count: Int {
        get{
            return storage.count
        }
    }
    
    init() {
        storage = []
    }
    
    init(array: [Int]) {
        storage = array
    }
    
    mutating func push(_ element: Int) {
        storage.append(element)
    }
    
    mutating func pop() -> Int {
        let element = self.storage[count - 1]
        self.storage = [Int](self.storage[0..<count - 1])
        return element
    }
}

struct ProtocolQueue : FirstCourseThirdTaskChecker.ArrayInitializable,
                      FirstCourseThirdTaskChecker.StorageProtocol {
    private var storage: [Int]
    var count: Int {
        get {
            return storage.count
        }
    }
    
    init() {
        storage = []
    }
    
    init(array: [Int]) {
        storage = array
    }
        
    mutating func push(_ element: Int) {
        storage.append(element)
    }
    
    mutating func pop() -> Int {
        let element = self.storage[0]
        self.storage = [Int](self.storage[1..<count])
        return element
    }
}

checker.checkProtocols(stack: ProtocolStack(), queue: ProtocolQueue())

extension User : JSONSerializable, JSONInitializable {
    public convenience init(JSON: String) {
        self.init()
        var parseFlag: Bool = false
        var parsedData = [String]()
        var currentValue = ""
        
        for symb in JSON {
            if symb == ":" {
                parseFlag = true
                currentValue = ""
                continue
            }
            else if symb == "," || symb == "}" {
                parseFlag = false
                parsedData.append(currentValue)
            }
            
            if parseFlag {
                currentValue.append(symb)
            }
        }
        
        fullName = String(parsedData[0][parsedData[0].index(parsedData[0].startIndex, offsetBy: 2)..<parsedData[0].index(before: parsedData[0].endIndex)])
        email = String(parsedData[1][parsedData[1].index(parsedData[1].startIndex, offsetBy: 2)..<parsedData[1].index(before: parsedData[1].endIndex)])
    }
    
    public func toJSON() -> String {
        return "{\"fullName\": \"" + fullName + "\", \"email\": \"" + self.email + "\"}"
    }
}

checker.checkExtensions(userType: type(of: User()))


//
//  main.swift
//  FirstCourseFinalTask
//
//  Copyright © 2017 E-Legion. All rights reserved.
//

import Foundation
import FirstCourseFinalTaskChecker

struct User : UserProtocol {
    var id: Identifier
    
    var username: String
    
    var fullName: String
    
    var avatarURL: URL?
    
    var currentUserFollowsThisUser: Bool
    
    var currentUserIsFollowedByThisUser: Bool
    
    var followsCount: Int
    
    var followedByCount: Int
    
    init(initialData: UserInitialData, follows: Bool, followedBy: Bool,
         followsCount: Int, followedByCount: Int) {
        self.id = initialData.id
        self.username = initialData.username
        self.fullName = initialData.fullName
        self.avatarURL = initialData.avatarURL
        self.currentUserFollowsThisUser = follows
        self.currentUserIsFollowedByThisUser = followedBy
        self.followsCount = followsCount
        self.followedByCount = followedByCount
    }
}

class UsersStorage : UsersStorageProtocol {

    var count: Int {
        get {
            return usersData.count
        }
    }
    
    var usersData: [(UserInitialData, [GenericIdentifier<UserProtocol>])]
    var currentUserID: GenericIdentifier<UserProtocol>
    
    required init?(users: [UserInitialData], followers: [(GenericIdentifier<UserProtocol>, GenericIdentifier<UserProtocol>)], currentUserID: GenericIdentifier<UserProtocol>) {
        usersData = []
        
        self.currentUserID = currentUserID
        
        for user in users {
            var connection: (UserInitialData, [GenericIdentifier<UserProtocol>]) = (user, [])
            for follow in followers {
                if follow.1 == user.id {
                    connection.1.append(follow.0)
                }
            }
            usersData.append(connection)
        }
        
        var existence = false
        
        for user in usersData {
            if user.0.id == currentUserID {
                existence = true
                self.currentUserID = currentUserID
            }
        }
        
        if !existence {
            return nil
        }
    }
    
    func currentUserIsFollowing(userID id: GenericIdentifier<UserProtocol>) -> Bool {
        for publisher in usersData {
            if publisher.0.id == id {
                for followerID in publisher.1 {
                    if followerID == currentUserID {
                        return true
                    }
                }
                
                break
            }
        }
        
        return false
    }
    
    func currentUserIsFollowed(userID id: GenericIdentifier<UserProtocol>) -> Bool {
        for publisher in usersData {
            if publisher.0.id == currentUserID {
                if publisher.1.contains(id) {
                    return true
                }
                
                break
            }
        }
        
        return false
    }
    
    func getFollowersCount(userID id: GenericIdentifier<UserProtocol>) -> Int {
        var counter: Int?
        
        for publisher in usersData {
            if publisher.0.id == id {
                counter = publisher.1.count
            }
        }
        
        return counter!
    }
    
    func getFollowedBy(userID id: GenericIdentifier<UserProtocol>) -> Int {
        var counter: Int = 0
        
        for publisher in usersData {
            if publisher.1.contains(id)
            {
                counter += 1
            }
        }
        
        return counter
    }
    
    func currentUser() -> UserProtocol {
        var currentUser: User?
        
        for user in usersData {
            if user.0.id == currentUserID
            {
                currentUser = User(initialData: user.0, follows: false, followedBy: false,
                                   followsCount: getFollowedBy(userID: currentUserID),
                                   followedByCount: getFollowersCount(userID: currentUserID))
            }
        }
        
        return currentUser!
    }
    
    func user(with userID: GenericIdentifier<UserProtocol>) -> UserProtocol? {
        var currentUser: User?
        
        for user in usersData {
            if user.0.id == userID
            {
                currentUser = User(initialData: user.0,
                                   follows: currentUserIsFollowing(userID: userID),
                                   followedBy: currentUserIsFollowed(userID: userID),
                                   followsCount: getFollowedBy(userID: userID),
                                   followedByCount: getFollowersCount(userID: userID))
            }
        }
        
        return currentUser
    }
    
    func findUsers(by searchString: String) -> [UserProtocol] {
        var finded: [UserProtocol] = []
        
        for publisher in usersData {
            if publisher.0.username.contains(searchString) ||
                publisher.0.fullName.contains(searchString)
            {
                finded.append(User(initialData: publisher.0,
                                   follows: currentUserIsFollowing(userID: publisher.0.id),
                                   followedBy: currentUserIsFollowed(userID: publisher.0.id),
                                   followsCount: getFollowedBy(userID: currentUserID),
                                   followedByCount: getFollowersCount(userID: currentUserID)))
            }
        }
        
        return finded
    }
    
    func follow(_ userIDToFollow: GenericIdentifier<UserProtocol>) -> Bool {
        var flag = false
        
        for i in 0..<usersData.count {
            if usersData[i].0.id == userIDToFollow {
                flag = true
                if !usersData[i].1.contains(currentUserID) {
                    usersData[i].1.append(currentUserID)
                }
            }
        }
        
        return flag
    }
    
    func unfollow(_ userIDToUnfollow: GenericIdentifier<UserProtocol>) -> Bool {
        var flag = false
        
        for i in 0..<usersData.count {
            if usersData[i].0.id == userIDToUnfollow {
                flag = true
                if usersData[i].1.contains(currentUserID) {
                    usersData[i].1.removeAll(where: {$0 == currentUserID})
                }
            }
        }
        
        return flag
    }
    
    func getUserByID(id userID: GenericIdentifier<UserProtocol>) -> UserProtocol {
        var user: UserProtocol?
        
        for publisher in usersData {
            if publisher.0.id == userID {
                user = User(initialData: publisher.0,
                    follows: currentUserIsFollowing(userID: publisher.0.id),
                    followedBy: currentUserIsFollowed(userID: publisher.0.id),
                    followsCount: getFollowedBy(userID: currentUserID),
                    followedByCount: getFollowersCount(userID: currentUserID))
            }
        }
        
        return user!
    }
    
    func usersFollowingUser(with userID: GenericIdentifier<UserProtocol>) -> [UserProtocol]? {
        var followers: [UserProtocol]?
        
        for publisher in usersData {
            if publisher.0.id == userID {
                followers = []
                let followersIDs: [GenericIdentifier<UserProtocol>] = publisher.1
                
                for id in followersIDs {
                    followers?.append(getUserByID(id: id))
                }
            }
        }
        
        return followers
    }
    
    func usersFollowedByUser(with userID: GenericIdentifier<UserProtocol>) -> [UserProtocol]? {
        var publishers: [UserProtocol]?
        
        for user in usersData {
            if user.1.contains(userID) {
                if publishers == nil {
                    publishers = []
                }
                
                publishers?.append(User(initialData: user.0,
                                       follows: currentUserIsFollowing(userID: user.0.id),
                                       followedBy: currentUserIsFollowed(userID: user.0.id),
                                       followsCount: getFollowedBy(userID: currentUserID),
                                       followedByCount: getFollowersCount(userID: currentUserID)))
            }
        }
        
        return publishers
    }
}

struct Post : PostProtocol {
    var id: Identifier
    
    var author: GenericIdentifier<UserProtocol>
    
    var description: String
    
    var imageURL: URL
    
    var createdTime: Date
    
    var currentUserLikesThisPost: Bool
    
    var likedByCount: Int
    
    init(initialData: PostInitialData,
         currentUserLikesThisPost: Bool,
         likedByCount: Int) {
        id = initialData.id
        author = initialData.author
        description = initialData.description
        imageURL = initialData.imageURL
        createdTime = initialData.createdTime
        self.currentUserLikesThisPost = currentUserLikesThisPost
        self.likedByCount = likedByCount
    }
}

class PostsStorage : PostsStorageProtocol{
    // массив кортежей (id публикации, [лайки к публикации])
    private var postsLikes: [GenericIdentifier<PostProtocol> : [GenericIdentifier<UserProtocol>]]
    private var posts: [GenericIdentifier<PostProtocol> : PostInitialData]
    private var currentUserID: GenericIdentifier<UserProtocol>
    
    required init(posts: [PostInitialData], likes: [(GenericIdentifier<UserProtocol>, GenericIdentifier<PostProtocol>)], currentUserID: GenericIdentifier<UserProtocol>) {
        self.currentUserID = currentUserID
        self.posts = [:]
        self.postsLikes = [:]
        
        for post in posts {
            self.posts[post.id] = post
        }
        
        for connection in likes {
            if postsLikes.keys.contains(connection.1) {
                postsLikes[connection.1]?.append(connection.0)
            }
            else {
                postsLikes[connection.1] = [connection.0]
            }
        }
    }
    
    var count: Int {
        get {
            return posts.count
        }
    }
    
    func currentUserLikes(postID: GenericIdentifier<PostProtocol>) -> Bool {
        if posts[postID] == nil {
            return false
        }
        
        if postsLikes[postID] == nil {
            return false
        }
        else if postsLikes[postID]!.contains(currentUserID) {
            return true
        }
        else {
            return false
        }
    }
    
    func getLikesNumber(postID: GenericIdentifier<PostProtocol>) -> Int {
        let likes = postsLikes[postID]
        
        if likes == nil {
            return 0
        }
        else {
            return likes!.count
        }
    }
    
    func post(with postID: GenericIdentifier<PostProtocol>) -> PostProtocol? {
        let post: PostInitialData? = posts[postID]
        
        if post == nil {
            return nil
        }
        else {
            return Post(initialData: post!,
                        currentUserLikesThisPost: currentUserLikes(postID: postID),
                        likedByCount: getLikesNumber(postID: postID))
        }
    }
    
    func findPosts(by authorID: GenericIdentifier<UserProtocol>) -> [PostProtocol] {
        var allUserPosts: [PostProtocol] = []
        
        for post in posts {
            if post.value.author == authorID {
                allUserPosts.append(Post(initialData: post.value,
                                         currentUserLikesThisPost: currentUserLikes(postID: post.key),
                                         likedByCount: getLikesNumber(postID: post.key)))
            }
        }
        
        return allUserPosts
    }
    
    func findPosts(by searchString: String) -> [PostProtocol] {
        var searchResult: [PostProtocol] = []
        
        for post in posts {
            if post.value.description.contains(searchString) {
                searchResult.append(Post(initialData: post.value,
                                         currentUserLikesThisPost: currentUserLikes(postID: post.key),
                                         likedByCount: getLikesNumber(postID: post.key)))
            }
        }
        
        return searchResult
    }
    
    func likePost(with postID: GenericIdentifier<PostProtocol>) -> Bool {
        if posts[postID] == nil {
            return false
        }
        else {
            if postsLikes[postID] == nil
            {
                postsLikes[postID] = [currentUserID]
            }
            else {
                postsLikes[postID]?.append(currentUserID)
            }
            
            return true
        }
    }
    
    func unlikePost(with postID: GenericIdentifier<PostProtocol>) -> Bool {
        if posts[postID] == nil {
            return false
        }
        else {
            if postsLikes[postID] == nil
            {
                postsLikes[postID] = []
            }
            else {
                postsLikes[postID]?.removeAll(where: {$0 == currentUserID})
            }
            
            return true
        }
    }
    
    func usersLikedPost(with postID: GenericIdentifier<PostProtocol>) -> [GenericIdentifier<UserProtocol>]? {
        let post = posts[postID]
        
        if post == nil {
            return nil
        }
        else {
            if postsLikes[postID] == nil {
                postsLikes[postID] = []
                return []
            }
            else {
                return postsLikes[postID]
            }
        }
    }
}

let checker = Checker(usersStorageClass: UsersStorage.self,
                      postsStorageClass: PostsStorage.self)

checker.run()


//
//  CalculatorViewController.swift
//  Course2Week3Task1
//
//  Copyright © 2018 e-Legion. All rights reserved.
//

import UIKit

class CalculatorViewController: UIViewController {
    
    @IBOutlet weak var resultLabel: UILabel!
    @IBOutlet weak var calculateBtn: UIButton!
    
    @IBOutlet weak var myStepper: UIStepper!
    @IBOutlet weak var stepperLabel: UILabel!
    @IBOutlet weak var firstOperandLabel: UILabel!
    
    @IBOutlet weak var mySlider: UISlider!
    @IBOutlet weak var sliderLabel: UILabel!
    @IBOutlet weak var secondOperandLabel: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initResultArea()
        
        let basicOffsetX = resultLabel.frame.origin.x
        
        initStepperArea(basicOffsetX)
        initSliderArea(basicOffsetX)
        
        
    }
    
    @IBAction func stepperPressed(_ sender: UIStepper) {
        let fraction = 4
        firstOperandLabel.text = formatNumber(sender.value, fraction, fraction)
        firstOperandLabel.sizeToFit()
    }
    
    @IBAction func sliderMoved(_ sender: UISlider) {
        let fraction = 4
        secondOperandLabel.text = formatNumber(Double(sender.value), fraction, fraction)
        secondOperandLabel.sizeToFit()
    }
    
    @IBAction func calculate(_ sender: UIButton) {
        if let stepperValue = firstOperandLabel.text,
            let sliderValue = secondOperandLabel.text {
            
            if let firstValue = Double(stepperValue),
                let secondValue = Double(sliderValue) {
                
                let result = formatNumber((firstValue + secondValue), 0, 4)
                resultLabel.text = result
            }
            
        }
    }
    
    func initStepperArea(_ basicOffsetX: CGFloat) {
        
        // a lot of magic constants...is that normal???!?!
        
        // Set stepper label positions
        
        stepperLabel.frame =
            CGRect(
                x: basicOffsetX,
                y: (resultLabel.frame.size.height + resultLabel.frame.origin.y + 32),
                width: 0,
                height: 0)
        
        stepperLabel.sizeToFit()
        
        // Set first operand label positions
        
        firstOperandLabel.frame =
            CGRect(
                x: basicOffsetX,
                y: stepperLabel.frame.origin.y + stepperLabel.frame.size.height + 24,
                width: 0,
                height: 0)
        
        firstOperandLabel.sizeToFit()
        
        // Set stepper postitions and colors
        
        myStepper.stepValue = 0.5
        myStepper.minimumValue = 1.0
        myStepper.maximumValue = 10.0
        stepperPressed(myStepper)
        
        let origin =
            CGPoint(
                x: resultLabel.frame.size.width - 80,
                y: stepperLabel.frame.origin.y + stepperLabel.frame.size.height + 16)
        
        let size = CGSize(width: 100, height: 100)
        
        myStepper.frame = CGRect(origin: origin, size: size)
        
        myStepper.layer.borderWidth = 1
        myStepper.layer.cornerRadius = 5
        myStepper.layer.borderColor = UIColor.orange.cgColor
        myStepper.setDecrementImage(myStepper.decrementImage(for: .normal), for: .normal)
        myStepper.setIncrementImage(myStepper.incrementImage(for: .normal), for: .normal)
    }
    
    func initSliderArea(_ basicOffsetX: CGFloat) {
        
        // Slider label positions
        
        sliderLabel.frame =
            CGRect(
                x: basicOffsetX,
                y: myStepper.frame.origin.y + myStepper.frame.size.height + 32,
                width: 0,
                height: 0)
        
        sliderLabel.sizeToFit()
        
        // second operand label positions
        
        secondOperandLabel.frame =
            CGRect(
                x: basicOffsetX,
                y: sliderLabel.frame.origin.y + sliderLabel.frame.size.height + 16,
                width: 0,
                height: 0)
        
        secondOperandLabel.sizeToFit()
        
        // slider formating
        
        mySlider.minimumValue = 1.0
        mySlider.maximumValue = 100.0
        sliderMoved(mySlider)
        
        mySlider.frame =
            CGRect(
                x: resultLabel.frame.size.width - 80,
                y: secondOperandLabel.frame.origin.y - 14,
                width: 90,
                height: 50)
    }
    
    func initResultArea() {
        
        // init result label format
        
        let origin = CGPoint(x: 16, y: 32)
        let size = CGSize(width: view.frame.width - 32, height: 60)
        
        resultLabel.frame = CGRect(origin: origin, size: size)
        resultLabel.textAlignment = .right
        resultLabel.text = "1"
        
        // init calculate button positions
        
        calculateBtn.frame =
            CGRect(
                x: resultLabel.frame.origin.x,
                y: view.frame.size.height - 72,
                width: resultLabel.frame.width,
                height: 60)
        
        calculateBtn.backgroundColor = UIColor.orange
    }
    
    func formatNumber(_ number: Double, _ minFraction: Int, _ maxFraction: Int) -> String {
        let formatter = NumberFormatter()
        formatter.minimumFractionDigits = minFraction
        formatter.maximumFractionDigits = maxFraction
        
        if let number = formatter.number(from: String(number)) {
            if let formatted = formatter.string(from: number) {
                return formatted
            }
        }
        
        return "NaN"
    }
}

//
//  main.swift
//  FirstCourseSecondTask
//
//  Copyright © 2017 E-Legion. All rights reserved.
//

import Foundation
import FirstCourseSecondTaskChecker


let checker = Checker()

func parseArray(array: [Int]) -> (Int, Int)
{
    var result = (0, 0)
    
    for elem in array
    {
        if (elem % 2 == 0)
        {
            result.0 += 1
        }
        else
        {
            result.1 += 1
        }
    }
    
    return result
}
 
checker.checkFirstFunction(function: parseArray)

func filterCircles(circles: [Checker.Circle]) -> [Checker.Circle]
{
    var result = [Checker.Circle]()
    var copy = circles.filter() { $0.color != Checker.Color.red }
    var whiteGroup = [Checker.Circle]()
    var blackGroup = [Checker.Circle]()
    var blueGroup = [Checker.Circle]()
    
    for i in 0..<copy.count
    {
        if (copy[i].color == Checker.Color.white)
        {
            whiteGroup.append(copy[i])
        }
        else if (copy[i].color == Checker.Color.black)
        {
            copy[i].radius *= 2
            blackGroup.append(copy[i])
        }
        else if (copy[i].color == Checker.Color.blue ||
                 copy[i].color == Checker.Color.green)
        {
            copy[i].color = Checker.Color.blue
            blueGroup.append(copy[i])
        }
    }
    
    result = whiteGroup + blackGroup + blueGroup
    
    return result
}

checker.checkSecondFunction(function: filterCircles)

func transform(employes: [Checker.EmployeeData]) -> [Checker.Employee]
{
    var result = [Checker.Employee]()
    
    /*
    for dict in employes
    {
        for (key, value) in dict
        {
            print(key, " - key")
            print(value, " - value")
        }
    }
    
    print()
    */
    
    for person in employes
    {
        let size = person.count
        
        if (size != 3)
        {
            continue
        }
        
        let keys = Array(person.keys)
        
        if !keys.contains("fullName") ||
           !keys.contains("salary") ||
           !keys.contains("company")
        {
            continue
        }
        
        result.append(Checker.Employee(fullName: person["fullName"]!,
                                       salary: person["salary"]!,
                                       company: person["company"]!))
    }
    
    return result
}

checker.checkThirdFunction(function: transform)

func packaging(names: [String]) -> [String : [String]]
{
    var result = [String : [String]]()
    
    for name in names
    {
        if !result.keys.contains(String(name.first!))
        {
            result[String(name.first!)] = [name]
        }
        else
        {
            if !result[String(name.first!)]!.contains(name)
            {
                result[String(name.first!)]!.append(name)
            }
        }
    }
    
    for (key, value) in result
    {
        if value.count < 2
        {
            result.removeValue(forKey: key)
        }
        else
        {
            result[key]!.sort(by: >)
        }
    }
    
    return result
}

checker.checkFourthFunction(function: packaging)

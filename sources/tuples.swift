// 6

/*
let name: String = "Vlad"
var age: UInt8 = 20

var cortege: (name: String, age: UInt8)

//print(cortege)

cortege.name = name
cortege.age = age

print(cortege)
*/

// 7

/*
var tuple: (Int16, Int16, Int16)

let (l1, l2) = (20, 30)

tuple.0 = Int16(l1)
tuple.1 = Int16(l2)
tuple.2 = tuple.0 + tuple.1

print(tuple)
*/

// 8

/*
var someTuple = (myName: "Alex", myAge: 12, "Russia")

print(someTuple.myName)
print(someTuple.2)
print(someTuple)
*/

// 9 

/*
var tuple: (first: Int8, second: Bool)

tuple.first = 20
tuple.second = true

print(tuple)
*/

// 10

/*
var (first, second) = (12, 21)

print(first, second)
(first, second) = (second, first)
print(first, second)

*/

//11

var tupleLovely: (film: String, number: Int) = ("Interstellar", 33)
let (lonelyFilm, lovelyNumber) = tupleLovely

print(tupleLovely)

typealias Man = (film: String, number: Int)

var anotherLovaly: Man = ("1+1", 7)

print(anotherLovaly)

var buffer: Man = tupleLovely

tupleLovely = anotherLovaly
anotherLovaly = buffer

print(tupleLovely)
print(anotherLovaly)

var result: (Int, Int, Int) = (tupleLovely.number, anotherLovaly.number, tupleLovely.number - anotherLovaly.number)

print(result)

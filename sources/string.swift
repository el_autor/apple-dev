// 1

/*
var str: String = "Swift"
let symb: Character = "Z"

var arr: Array<String> = Array(repeating: String(symb), count: str.count)
print("\(arr)\n")

str = String(symb)
print(str)
*/

// 2

/*
let str: String = "JohnWick"

var first = str.startIndex
print(str[first])
print(type(of: first))

var end = str.index(before: str.endIndex) 
print(str[end])

var str1 = "Бабка-ежка"
var sub = str1[str1.startIndex...str1.index(str1.startIndex, offsetBy: 3)]
print(sub)
var firstSub = sub.startIndex
// sub[firstSub] = "f" readonly
print(sub)
print(str1) 
print(type(of : sub))
*/

// 3


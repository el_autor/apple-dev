// 3

/*
var range: ClosedRange<Int> = 1...9
print(range)
print(type(of: range))

var range2: Range<Int> = 1..<10
print(range2)
print(type(of: range2))
*/

// 8

/*
var range = -100...100
var value: UInt = 21
var flag = range.contains(Int(value))
print(flag)
*/

// 9

/*
var alphabet = "A"..."Z"
//print(alphabet.min())
//print(alphabet.max())
print(alphabet.lowerBound)
print(alphabet.upperBound)
*/

// 10

/*
var range1 = 1.0...5.001
print(range1)
var range2 = Float(1)...5.0001
print(range2)
*/
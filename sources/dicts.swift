// 4

/*
var firstDict: Dictionary<Int, String> = [1 : "first", 2 : "second", 3 : "third"]

var dictKeys = firstDict.keys
var dictValues = firstDict.values

var secondDict: Dictionary<Int, String> = Dictionary(uniqueKeysWithValues: zip(dictKeys, dictValues))

print(secondDict)
*/

// 5
    
var myDict: Dictionary<Int, Array<String>> = [1 : ["fdf", "ffdf"], 2 : ["dsds", "sds"]]

var elements = myDict.count

var array = Array(repeating: elements, count: elements)

print(type(of: array))
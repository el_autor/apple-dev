// 4 

/*
var arr: Array<Int> = [1, 2, 3, 4]
arr[1...2] = [arr[0] + arr[3]]
arr.sort()
print(arr)
*/

// 5

/*
var arr = Array(1...100)
var emptyArr: [Int] = []

emptyArr = Array(arr[25...50])

print(emptyArr)
*/

// 6

/*
var arr: [Character] = ["a", "b", "c", "d", "e"]
arr.insert("z", at: 1)

var total: UInt8 = UInt8(arr.count)
print(total)
*/

// 9

/*
var arr = Array<Array<Character>>()
arr.append(["a", "b", "c"])
arr.append(["d", "e", "f"])
arr.remove(at:1)
var arr2 = arr.remove(at: 0)
print(arr)
print(arr2)
print(arr2[0])
*/
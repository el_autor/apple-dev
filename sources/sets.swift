// 6

var set1: Set<Int> = Set(1...10)
var set2: Set<Int> = Set(5...15)

var set3: Set = set1.intersection(set2)
var set4: Set = set1.symmetricDifference(set2)

var answer: UInt8 = UInt8(set4.count)
print(answer)

print(set4)
// set4 = set4.sort() error!
print(set4)
final class Presenter {
    
    struct Person {
        let firstName: String
        let lastName: String
    }
    
    weak var view: ViewControllerInput!
    
    private let person: Person
    
    init() {
        person = Person(firstName: "Vlad", lastName: "Krivozubov")
    }
}

extension Presenter: ViewControllerOutput {
    func didTapButton() {
        let text = "Hello, \(person.firstName) \(person.lastName)"
        
        view?.setGreetingText(text: text)
    }
}

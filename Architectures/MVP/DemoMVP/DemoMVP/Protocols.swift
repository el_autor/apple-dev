protocol ViewControllerOutput: AnyObject {
    func didTapButton()
}

protocol ViewControllerInput: AnyObject {
    func setGreetingText(text: String)
}

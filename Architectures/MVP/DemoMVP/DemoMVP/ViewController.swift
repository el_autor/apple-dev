import UIKit

final class ViewController: UIViewController {

    var output: ViewControllerOutput!
    
    @IBOutlet weak var greetingTitle: UILabel!
    
    @IBOutlet weak var greetingButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .blue
        
        greetingButton.addTarget(self,
                                 action: #selector(didTapButton),
                                 for: .touchUpInside)
    }

    @objc
    private func didTapButton() {
        output?.didTapButton()
    }
}

extension ViewController: ViewControllerInput {
    func setGreetingText(text: String) {
        greetingTitle.text = text
    }
}

